package com.andtinder.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.andtinder.R;
import com.andtinder.model.CardModel;

public final class SimpleCardStackAdapter extends CardStackAdapter {

	private AQuery aQuery;
	Context mContext;

	public SimpleCardStackAdapter(Context mContext) {
		super(mContext);

	}
	public void setmContext(Context context){
		this.mContext =context;
	}

	@Override
	public View getCardView(int position, CardModel model, View convertView, ViewGroup parent) {
		aQuery = new AQuery(mContext);
		if(convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.std_card_inner, parent, false);
			assert convertView != null;
		}

		ImageView img = (ImageView)convertView.findViewById(R.id.image);

		String url =model.getImg();

		try {

			aQuery.id(img).image(url, true, true, 0, 0, new BitmapAjaxCallback() {

				@Override
				protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
					//super.callback(url, iv, bm, status);
					iv.setImageBitmap(bm);
				}
			});

		} catch (NullPointerException e) {
			url = "";
			e.printStackTrace();

		}
				//((ImageView) convertView.findViewById(R.id.image)).setImageDrawable(model.getCardImageDrawable());
		/*((TextView) convertView.findViewById(R.id.title)).setText(model.getTitle());
		*//*((TextView) convertView.findViewById(R.id.description)).setText(model.getDescription());*/

		return convertView;
	}
}
