package com.example.saket.postornotapp.parser;

import com.example.saket.postornotapp.entity.CreateUserEntity;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mobiweb on 12/12/15.
 */
public class CreateUserParser {

    private static CreateUserParser instance;
    private boolean parseStatus = true;

    ArrayList<CreateUserEntity>rspncVal = new ArrayList<CreateUserEntity>();

    public static CreateUserParser getInstance(){

        if (instance == null){

            instance = new CreateUserParser();
        }
        return instance;
    }

    public boolean parseResponse(String jsonResponse){
        parseStatus = true;
        JSONObject jsonObj;
        try{
            jsonObj = new JSONObject(jsonResponse);

            CreateUserEntity login=new CreateUserEntity();

            login.setCode(jsonObj.getInt("code"));

            login.setMessage(jsonObj.getString("message"));

            JSONObject jsnResponObj = jsonObj.getJSONObject("response");
            JSONObject jsonuser_data=jsnResponObj.getJSONObject("user_data");

            login.setUser_id(jsonuser_data.getString("user_id"));
            login.setFacebook_id(jsonuser_data.getString("facebook_id"));
            login.setGoogle_id(jsonuser_data.getString("google_id"));
            login.setInstagram_access_token(jsonuser_data.getString("instagram_access_token"));
            login.setFull_name(jsonuser_data.getString("full_name"));
            login.setEmail(jsonuser_data.getString("email"));
            login.setPassword(jsonuser_data.getString("password"));
            login.setPassword_reset_key(jsonuser_data.getString("password_reset_key"));
            login.setProfile_pic(jsonuser_data.getString("profile_pic"));
            login.setGender(jsonuser_data.getString("gender"));
            login.setDescription(jsonuser_data.getString("description"));
            login.setPhone_number(jsonuser_data.getString("phone_number"));
            login.setConnected_via(jsonuser_data.getString("connected_via"));
            login.setUser_status(jsonuser_data.getString("user_status"));
            login.setDevice_type(jsonuser_data.getString("device_type"));
            login.setDevice_id(jsonuser_data.getString("device_id"));
            login.setDevice_token(jsonuser_data.getString("device_token"));
            login.setDate_created(jsonuser_data.getString("date_created"));

            rspncVal.add(login);
            CreateUserEntity.setInstance(login);
            CreateUserEntity.getInstance().setValue(rspncVal);



        }catch(Exception e){
            e.printStackTrace();
            parseStatus = false;
        }

        return parseStatus;

    }
}
