package com.example.saket.postornotapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.example.saket.postornotapp.R;
import com.example.saket.postornotapp.entity.CreateUserEntity;
import com.example.saket.postornotapp.entity.Get_all_PostingEntity;
import com.example.saket.postornotapp.entity.UserDetailEntity;

import java.util.ArrayList;

/**
 * Created by mobiweb on 23/12/15.
 */
public class GridRyciclerAdapter extends RecyclerView.Adapter<GridRyciclerAdapter.ViewHolder> {
    private Context mContext;

    private AQuery aQuery;
    ArrayList<Get_all_PostingEntity> nm ;
    private static MyClickListener myClickListener;
    String layOut;

    public GridRyciclerAdapter(Context context, ArrayList<Get_all_PostingEntity> arr,String layOu){
        mContext=context;
        nm=arr;
        layOut = layOu;

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ViewHolder viewHolder;
        if (layOut.equals("grid")){
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.custom_grdvw_card, viewGroup, false);
           viewHolder = new ViewHolder(v);
        }else {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.cstm_crd_lstve, viewGroup, false);
            viewHolder = new ViewHolder(v);
        }


        aQuery = new AQuery(mContext);

        return viewHolder;
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    @Override
    public void onBindViewHolder( final ViewHolder holder, int position) {
        String url = nm.get(position).getAttachment_url();


        try {

            aQuery.id(holder.usrImg).image(url, true, true, 0, 0, new BitmapAjaxCallback() {

                @Override
                protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                    //super.callback(url, iv, bm, status);
                    iv.setImageBitmap(bm);
                }
            });

        } catch (NullPointerException e) {
            url = "";
            e.printStackTrace();

        }

    }

    @Override
    public int getItemCount() {
        return nm.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private final ImageView usrImg;
        private final TextView txtPost;
        private final TextView txt_postNot;
        private final ImageView img_share;

        public ViewHolder(View itemView) {
            super(itemView);

            usrImg =(ImageView)itemView.findViewById(R.id.imgVww);
            txtPost=(TextView)itemView.findViewById(R.id.txt_post);
            txt_postNot =(TextView)itemView.findViewById(R.id.txt_postNot);
            img_share =(ImageView)itemView.findViewById(R.id.img_share);

            /*tvspecies=(TextView)itemView.findViewById(R.id.txtMvTitl);
            linearLayout=(LinearLayout)itemView.findViewById(R.id.imGrdVw);*/
        }
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
