package com.example.saket.postornotapp.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.andtinder.model.CardModel;
import com.andtinder.view.CardContainer;
import com.andtinder.view.SimpleCardStackAdapter;
import com.aviary.android.feather.library.Constants;
import com.aviary.android.feather.sdk.FeatherActivity;
import com.example.saket.postornotapp.R;
import com.example.saket.postornotapp.entity.CreateUserEntity;
import com.example.saket.postornotapp.entity.Get_all_PostingEntity;
import com.example.saket.postornotapp.entity.UserDetailEntity;
import com.example.saket.postornotapp.netcom.api.TaskCompleteListener;
import com.example.saket.postornotapp.netcome.CheckNetworkStateClass;
import com.example.saket.postornotapp.netcome.RequestResponseUpdtd;
import com.example.saket.postornotapp.sharedpref_db.SharedPrfencesDb;
import com.example.saket.postornotapp.utils.ConstantLib;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import lombok.val;

/**
 * Created by Saket on 14/12/2015.
 */
public class HomeActivity extends AppCompatActivity implements TaskCompleteListener {

    Context context;
    private Toolbar tooolBar;
    private ImageView imgUsrProf;
    private ImageView notific;
    private ImageView sttng;
    private CardContainer mCardContainer;
    private ImageView imgSmile;
    private ImageView imgNotPost;
    ArrayList<CardModel> mdata;
    int i = 0;
    private ImageView bttnPlusCamera;
    private String selectedImagePath;

    private static final int REQUEST_CAMERA = 100;
    private static final int SELECT_FILE = 200;
    private static final int IMAGE_EDIT=1;

    private String bs = "network error";
    private Intent dataa;
    private SharedPrfencesDb sp;

    ArrayList<CreateUserEntity> loginDtl = new ArrayList<CreateUserEntity>();
    ArrayList<CardModel>cmd = new ArrayList<CardModel>();
    private String usrDtl = "";
    private ArrayList<Get_all_PostingEntity> val;
    private String suc = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.home_activity);
        context = this;

        tooolBar = (Toolbar) findViewById(R.id.tllbar);
        imgSmile = (ImageView) findViewById(R.id.imgSmile);

        setSupportActionBar(tooolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        //getSupportActionBar().setIcon(R.mipmap.ic_post_or_not_logo_48dp);

        imgUsrProf = (ImageView) findViewById(R.id.imgProfl);
        notific = (ImageView) findViewById(R.id.imgNtfctn);
        sttng = (ImageView) findViewById(R.id.imgSttng);
        imgNotPost = (ImageView) findViewById(R.id.imgNotPost);
        bttnPlusCamera = (ImageView) findViewById(R.id.imgPlusBttn);

        sp = new SharedPrfencesDb(context,SharedPrfencesDb.Remember_me);

        loginDtl = CreateUserEntity.getInstance().getValue();
        cmd= CardModel.getData();

        if (loginDtl != null) {
            for (CreateUserEntity cr : loginDtl) {
                usrDtl = cr.getUser_id();
            }
        }
        if (!usrDtl.equals(""))
        {
            sp.saveUserId(usrDtl,SharedPrfencesDb.UserId);
        }else {
            usrDtl = sp.getUsrId(context,SharedPrfencesDb.Remember_me,SharedPrfencesDb.UserId);
        }

        imgUsrProf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // finish();

                if (CheckNetworkStateClass.isOnline(context)) {

                    RequestResponseUpdtd.getInstance().performRequest(context, ConstantLib.BASE_URL + ConstantLib.URL_GETAL_POSTINGS, ConstantLib
                            .REQ_TYPE_GET_ALL_POSTINGS, "POST",usrDtl,"50","0",usrDtl);//usrDtl-- will be used as a first param
                } else {

                    Toast toast = Toast.makeText(context, "Check Network ", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                }
            }

        });
        notific.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Notification_Activity.class);
                startActivity(intent);
            }
        });
        sttng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SettingActivity.class);
                startActivity(intent);
            }
        });
        mCardContainer = (CardContainer) findViewById(R.id.layoutview);

        Resources r = getResources();

        SimpleCardStackAdapter adapter = new SimpleCardStackAdapter(this);
        adapter.setmContext(this);
        adapter.addHomeArrayList(cmd);

        /*adapter.add(new CardModel("Title1", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title2", "Description goes here", r.getDrawable(R.drawable.picture2)));
        adapter.add(new CardModel("Title3", "Description goes here", r.getDrawable(R.drawable.picture3)));
        adapter.add(new CardModel("Title4", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title5", "Description goes here", r.getDrawable(R.drawable.picture2)));
        adapter.add(new CardModel("Title6", "Description goes here", r.getDrawable(R.drawable.picture3)));
        adapter.add(new CardModel("Title1", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title2", "Description goes here", r.getDrawable(R.drawable.picture2)));
        adapter.add(new CardModel("Title3", "Description goes here", r.getDrawable(R.drawable.picture3)));
        adapter.add(new CardModel("Title4", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title5", "Description goes here", r.getDrawable(R.drawable.picture2)));
        adapter.add(new CardModel("Title6", "Description goes here", r.getDrawable(R.drawable.picture3)));
        adapter.add(new CardModel("Title1", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title2", "Description goes here", r.getDrawable(R.drawable.picture2)));
        adapter.add(new CardModel("Title3", "Description goes here", r.getDrawable(R.drawable.picture3)));
        adapter.add(new CardModel("Title4", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title5", "Description goes here", r.getDrawable(R.drawable.picture2)));
        adapter.add(new CardModel("Title6", "Description goes here", r.getDrawable(R.drawable.picture3)));
        adapter.add(new CardModel("Title1", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title2", "Description goes here", r.getDrawable(R.drawable.picture2)));
        adapter.add(new CardModel("Title3", "Description goes here", r.getDrawable(R.drawable.picture3)));
        adapter.add(new CardModel("Title4", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title5", "Description goes here", r.getDrawable(R.drawable.picture2)));
        adapter.add(new CardModel("Title6", "Description goes here", r.getDrawable(R.drawable.picture3)));
        adapter.add(new CardModel("Title1", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title2", "Description goes here", r.getDrawable(R.drawable.picture2)));
        adapter.add(new CardModel("Title3", "Description goes here", r.getDrawable(R.drawable.picture3)));
        adapter.add(new CardModel("Title4", "Description goes here", r.getDrawable(R.drawable.picture1)));
        adapter.add(new CardModel("Title5", "Description goes here", r.getDrawable(R.drawable.picture2)));
*/

        //CardModel cardModel = new CardModel("Title1", "Description goes here", r.getDrawable(R.drawable.picture1));
        //CardModel cardModel = new CardModel();
        new CardModel().setOnClickListener(new CardModel.OnClickListener() {
            @Override
            public void OnClickListener() {
                Log.i("Swipeable Cards", "I am pressing the card");
                // Toast.makeText(getBaseContext(), "Card Pressed", Toast.LENGTH_LONG).show();
            }
        });

        new CardModel().setOnCardDismissedListener(new CardModel.OnCardDismissedListener() {
            @Override
            public void onLike() {
                Log.i("Swipeable Cards", "I like the card");
                //Toast.makeText(getBaseContext(), "I Like it", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onDislike() {
                Log.i("Swipeable Cards", "I dislike the card");
                //Toast.makeText(getBaseContext(), "I Don'tLike it", Toast.LENGTH_LONG).show();
            }
        });


        //adapter.add(cardModel);


        mCardContainer.setAdapter(adapter);

        imgSmile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    mCardContainer.OnClickSwipeCard(589.24414, -158.03989, 500, 1672.3754);

                } catch (IndexOutOfBoundsException e) {

                    Log.d("Index out of bond ", e.toString());

                } catch (NullPointerException f) {
                    Log.d("Index out of bond ", f.toString());
                }


            }
        });

        imgNotPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    mCardContainer.OnClickSwipeCard(-275.02966, -158.03989, 500, -2919.563);


                } catch (IndexOutOfBoundsException e) {

                    Log.d("Index out of bond ", e.toString());

                } catch (NullPointerException f) {
                    Log.d("Index out of bond ", f.toString());
                }


            }
        });
        bttnPlusCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo","Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");

                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            dataa = data;
            if (requestCode == REQUEST_CAMERA) {

                selectedImagePath = getImagePath();

                avairyLib(selectedImagePath);
                   //usrImage.setImageBitmap(decodeFile(selectedImagePath));

                // imageView.setImageBitmap(bitmapImage );

                // dataa= (Uri)data.getExtras().get("data");

              /*  Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();



                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/

                // img_pro.setImageBitmap(bitmapImage);

            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null,
                        null);
                // Move to first row
                cursor.moveToFirst();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                //cursor.moveToFirst();

                selectedImagePath = cursor.getString(column_index);

                cursor.close();
                System.out.println("Select path===============>" + selectedImagePath.toString());

                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);

                //usrImage.setImageBitmap(bm);

                avairyLib(selectedImagePath);
            }else if (requestCode == IMAGE_EDIT) {
                Uri mImageUri = data.getData();
                System.out.println("avairy uri-----------"+mImageUri.toString());
                setImageUri();
                //return bitmap of the mImageUri....using changeImageUriBitmap() method.
                //Bitmap bm = changeImageUriBitmap(mImageUri);

                Bundle extra = data.getExtras();
                if (null != extra) {
                    // image has been changed by the user?
                    boolean changed = extra.getBoolean(Constants.EXTRA_OUT_BITMAP_CHANGED);

                    new ImagePost_Activity(mImageUri);

                    Intent intent = new Intent(context,ImagePost_Activity.class);
                    startActivity(intent);
                }
            }
        }
    }
    //decode for the camera
    public void avairyLib(String imgPath){
        Intent newIntent = new Intent( this, FeatherActivity.class );
        newIntent.setData( Uri.parse(imgPath) );
        newIntent.putExtra( Constants.EXTRA_IN_API_KEY_SECRET, "d03b3eee-1598-4c56-b659-65d30f578c92" );
        startActivityForResult(newIntent, 1);
    }



    public Bitmap changeImageUriBitmap(Uri selectedImageUri){
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null,
                null);
        // Move to first row
        cursor.moveToFirst();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        //cursor.moveToFirst();

        selectedImagePath = cursor.getString(column_index);

        cursor.close();
        System.out.println("Select path===============>" + selectedImagePath.toString());

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        return bm;
    }


    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 90;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }

    // geting URi for the  camera save image
    public Uri setImageUri() {
        // Store image in dcim
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".Jpg");
        Uri imgUri = Uri.fromFile(file);

        System.out.println("avairy uri-----------"+imgUri.toString());

        selectedImagePath = file.getAbsolutePath();
        System.out.println("avairy uri-----------"+selectedImagePath);
        return imgUri;
    }

    public String getImagePath() {
        return selectedImagePath;
    }


    @Override
    public void taskCompleted(int taskType, boolean status) {
        switch (taskType) {

            case ConstantLib.REQ_TYPE_GET_ALL_POSTINGS:

                if (status) {
                    jsonLogin();
                    if (suc.equals("SUCCESS")) {
                        Toast.makeText(context, suc, Toast.LENGTH_SHORT).show();

                        if (status) {

                            //saveCheckState();
                            Intent intent = new Intent(context, UserProfile_Activity.class);
                            startActivity(intent);
                        }
                    } else {
                        Toast.makeText(context, suc, Toast.LENGTH_SHORT).show();
                    }

                }
                break;
        }

    }

    public void jsonLogin() {
        if (Get_all_PostingEntity.getAuthor_data() != null) {
            val = Get_all_PostingEntity.getAuthor_data();
            System.out.println("size of valllllllllll list===========>" + val.size());

            for (Get_all_PostingEntity log : val) {

                suc = log.getMessage();
                //msg = log.getSucc_msg();

            }
        }
    }
}
