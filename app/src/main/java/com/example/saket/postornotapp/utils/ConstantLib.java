package com.example.saket.postornotapp.utils;



import com.example.saket.postornotapp.netcom.api.TaskCompleteListener;

import java.util.LinkedList;
import java.util.regex.Pattern;

/*Constant interface for entire project.All the constant are declare here.*/

public interface ConstantLib {
	String LOG = "GAMEPLAN";
	//String PROJECT_NUMBER="201504907095";
	String PROJECT_NUMBER="962782720510"; 
	String DEVICE_TYPE = "android";
	//String URL_3 = "http://103.15.67.74/pro1/mobiapp/webservice/webservice.php?method=";
	//String URL_3 = "http://www.webservice.appretailer.com/webservice.php?method=";
	//String URL_3 = "http://appretailer.com/appbuilder/webservice/webservice.php?method=";

	String URL_3 = "http://syncappdata.com/postornot/api/";
	String BASE_URL = URL_3;

	int CONNECTION_TIMEOUT = 50000;
	int READTIMEOUT = 50000;
	/*
	 * Constant for the socket timeout for the requests
	 */
	int SOCKET_TIMEOUT = 50000;
	String API_TYPE = "API";
	String REQ_TYPE = "REQ_TYPE";

	//String URL_1 = "http://192.168.0.5/pro1/mobiapp/webservice/webservice_dev.php?method=";
	
	String URL_1 = "http://www.webservice.appretailer.com/webservice.php?method=";
	//Key of pages
	
	/*String key_of_Retailer ="mobiappsKcglq3eB";
	String key_of_Whole_Seller ="mobiappsuFjJZmR9";*/
	
	//for Omar Url
	/*String key_of_Retailer ="mobiapps2Do7KFkJ";
	String key_of_Whole_Seller ="mobiappsOL9WgjQP";
	
	String key_of_app =key_of_Whole_Seller ;
	//String key_of_app =key_of_Retailer ;
	public static final String MEDIA_PLAYER_KEY       =      key_of_app;//"mobiappsuFjJZmR9""mobiappsKcglq3eB";
	  public static final String PHOTO_GALLERY_KEY    =      key_of_app;//"mobiappsuFjJZmR9""mobiappsKcglq3eB";
	  public static final String PRODUCT_DETAIL_KEY   =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String LOCATION_ON_MAP_KEY  =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String HOMEPAGE_KEY         =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String HELP_US_KEY          =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String CUSTOM_KEY           =      key_of_app;///*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String ABOUTUS_KEY          =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String FORGET_PWD_KEY       =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String REG_KEY              =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String SHIPPIN_METHOD_KEY   =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String SHIPPING_ADDRESS_KEY =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String OFFLINE_PAYMENT_KEY  =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String EXISTING_ADDRESS_KEY =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String ACCOUNT_KEY          =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String SPLASH_KEY           =      key_of_app;//*//*"mobiappsuFjJZmR9"*//*"mobiappsKcglq3eB";
	  public static final String UPDATE_ADDRESS_KEY   =      key_of_app;//"mobiappsKcglq3eB";
	  public static final String COUPONS_KEY          =      key_of_app;//"mobiappsKcglq3eB";
	  public static final String STATE_KEY            =      key_of_app;
	  public static final String SHIPPING_CHARGES     =      key_of_app;
	  public static final String COUPONS_APPLIED      =      key_of_app;
	  public static final String APPOINTMENT2_KEY     =      key_of_app;
	  public static final String External_Link1Key    =      key_of_app;
	  public static final String ORDER_HISTORY_KEY    =      key_of_app;
	  public static final String External_Link6Key    =      key_of_app;

*/


	public static final byte REQ_TYPE_LOGIN_TYPE =(byte)1;
	public static final byte REQ_TYPE_SIGN_UP =(byte)2;
	public static final byte REQ_TYPE_FORGETPASS=(byte)3;
	public static final byte REQ_TYPE_RESETPASS=(byte)4;
	public static final byte REQ_TYP_GET_USRDETAIL=(byte)5;
	public static final byte REQ_TYPE_GET_ALL_POSTINGS=(byte)6;
	public static final byte REQ_TYPE_POST_IMAGES=(byte)7;



	  
	  
	  
	String WEB_URL = "WEB_URL";
	
	// Page Id
	String ABOUT_US_PAGE_ID ="217";
	String HELP_US_PAGE_ID  ="221";
	String LABOUR_DAY_PAGE_ID = "236";
	String CUSTOM_ABOUT_US_PAGE_ID ="223";
	String HEAT_PUMP_WATER_HEATER_PAGE_ID="239";
	String CUSTOM_1_PAGE_ID="234";
	String CUSTOM_2_PAGE_ID="240";

	/*
	 * Define all method here
	 */

	public static final String URL_LOGIN="users/login?";
	public static final String URL_REG="users/signup?";
	public static final String URL_FORGTPASS="users/forgot_password?";
	public static final String URL_RESETPASS ="election/seachelection?";
	public static final String URL_GETUSR_DTL="users/getuserdetail?";
	public static final String URL_GETAL_POSTINGS="posting/getallpostings?";
	public static final String URL_ADD_POSTING="posting/addposting?";
	

	  
	/*
	 * Constant for the track up the task that are to be requested
	 */
	LinkedList<TaskCompleteListener> TASK_COMPLETE_SET = new LinkedList<TaskCompleteListener>();

	/*
	 * Veriable for local preferences.
	 */

	/*
	 * Constant for regular expression pattern for validating the email
	 */
	public final static Pattern EMAIL_ADDRESS_PATTERN = Pattern
			.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{1,3}" + ")+");

	public static String RESPONSESUCESS = "success";
	public static final String amount = "amount";
	  public static final String app_name = "app_name";
	  public static final String application_type = "application_type";
	  public static final String backgrnd_home_imgs = "background_home_images";
	  public static final String btn_background_color = "btn_background_color";
	  public static final String btn_text_color = "btn_text_color";
	  public static final String city = "city";
	  public static final String method="method";
	  public static final String country = "country";
	  public static final String delivered_day ="delivered_day";
	  public static final String default_id = "default_id";
	  public static final String default_price = "default_price";
	  public static final String description = "description";
	  public static final String details = "details";
	  public static final String facebook = "facebook";
	  public static final String header_bg_color = "header_bg_color";
	  public static final String header_txt_color = "header_text_color";
	  public static final String heading = "heading";
	  public static final String id = "id";
	  public static final String images = "images";
	  public static final String increments_of = "increments_of";
	  public static final String lat = "lat";
	  public static final String layout = "layout";
	  public static final String logo = "logo";
	  public static final String longi = "long";
	  public static final String main_images = "main_images";
	  public static final String minimum_quantity = "minimum_quantity";
	  public static final String modified_on = "modified_on";
	  public static final String name = "name";
	  public static final String option = "option";
	  public static final String option_array = "option_array";
	  public static final String option_value_id = "option_value_id";
	  public static final String page_background = "background";
	  public static final String page_icon = "page_icon";
	  public static final String page_key = "page_key";
	  public static final String page_name = "page_name";
	  public static final String pages = "pages";
	  public static final String pages_id = "id";
	  public static final String phone_number = "phone_number";
	  public static final String photo_id = "photo_id";
	  public static final String price = "price";
	  public static final String priority = "priority";
	  public static final String product_number = "product_number";
	  public static final String quantity = "quantity";
	  public static final String required = "required";//required
	  public static final String result = "result";
	  public static final String search_bar_bckgrnd = "search_bar_background";
	  public static final String sequence = "sequence";
	  public static final String slider_menu = "slider_menu";
	  public static final String slider_menu_text_color = "slider_menu_text_color";
	  public static final String special_price = "special_price";
	  public static final String sub_header_colour = "sub_header_colour";
	  public static final String subcategory = "subcategory";
	  public static final String subcategory_count = "subcategory_count";
	  public static final String succ_msg = "msg";
	  public static final String taxamount = "taxamount";
	  public static final String text_body_colour = "text_body_colour";
	  public static final String text_colour = "h_text_colour";
	  public static final String theme_color = "theme_colour";
	  public static final String title = "title";
	  public static final String twitter = "twitter";
	  public static final String type = "type";
	  public static final String url = "url";
	  public static final String url_id = "url_id";
	  public static final String username = "username";
	  public static final String value = "value";
	  public static final String weight = "weight";
	  public static final String upcoming = "upcoming";
	  public static final String udid ="udid";
	  public static final String user_id ="user_id";
	  public static final String firstname = "firstname";
	  public static final String lastname ="lastname";
	  public static final String Address ="Address";
	  public static final String Country ="Country";
	  public static final String State ="State";
	  public static final String City = "City";
	  public static final String ZipCode ="ZipCode";
	  public static final String homephone="homephone";
	  public static final String workphone ="workphone";
	  public static final String address_type ="address_type";
	  public static final String Key ="Key";
	  public static final String middle_image ="middle_image";
	  public static final String middle_image_id ="middle_image_id";
	  public static final String middle_page_key ="middle_page_key";
	  public static final String middle_page_name ="middle_page_name";
	  public static final String bill_address ="bill_address";
	  public static final String ship_address ="ship_address";
	  public static final String cart="cart";
	  public static final String code ="code";
	  public static final String start_date="start_date";
	  public static final String end_date = "end_date";
	  public static final String target ="target";
	  public static final String product="product";
	  public static final String tier_pricing="tier_pricing";
	  public static final String group_id ="group_id";
	  public static final String from_quantity="from_quantity";
	  public static final String to_quantity ="to_quantity";
	  public static final String created_on ="created_on";
	  public static final String appointment = "appointment";
	  public static final String order_id = "order_id";
	  public static final String status = "status";
	  public static final String order_date = "order_date";
	  public static final String img_type ="img_type";
	  public static final String background ="background";
	  public static final String Page_url ="Page_url";
	  public static final String Page_type ="Page_type";
	 
	  
	  
	  
	  
	  
	  
	  
}