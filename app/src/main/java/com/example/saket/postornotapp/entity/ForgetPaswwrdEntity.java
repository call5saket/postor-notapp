package com.example.saket.postornotapp.entity;

import java.util.ArrayList;

/**
 * Created by Saket on 19/12/2015.
 */
public class ForgetPaswwrdEntity {

    private static ForgetPaswwrdEntity instanse;

    ArrayList<ForgetPaswwrdEntity>frg;
    public String message="",password_reset_key ="";

    public static ForgetPaswwrdEntity getInstanse() {
        if(instanse ==null){

            instanse = new ForgetPaswwrdEntity();
        }
        return instanse;
    }

    public static void setInstanse(ForgetPaswwrdEntity instanse) {
        ForgetPaswwrdEntity.instanse = instanse;
    }

    public ArrayList<ForgetPaswwrdEntity> getFrg() {
        return frg;
    }

    public void setFrg(ArrayList<ForgetPaswwrdEntity> frg) {
        this.frg = frg;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPassword_reset_key() {
        return password_reset_key;
    }

    public void setPassword_reset_key(String password_reset_key) {
        this.password_reset_key = password_reset_key;
    }
}
