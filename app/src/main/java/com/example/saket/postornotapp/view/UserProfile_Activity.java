package com.example.saket.postornotapp.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.example.saket.postornotapp.R;
import com.example.saket.postornotapp.adapter.GridRyciclerAdapter;
import com.example.saket.postornotapp.entity.Get_all_PostingEntity;
import com.pkmmte.view.CircularImageView;

import java.util.ArrayList;

/**
 * Created by Saket on 15/12/2015.
 */
public class UserProfile_Activity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgUsrProf;
    private ImageView notific;
    private ImageView sttng;

    Context context;
    private ImageView listSlctBtn;
    private ImageView gridBtn;
    private RecyclerView recyclerVw;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public ArrayList<Get_all_PostingEntity> authorArray = new ArrayList<Get_all_PostingEntity>();
    public ArrayList<Get_all_PostingEntity> postingData = new ArrayList<Get_all_PostingEntity>();
    private String fullName;
    private String proPic;
    private int addEd=0;
    private int sharEd=00;
    private AQuery aQuery;
    private TextView adddEd;
    private TextView shaRedd;
    private TextView usErName;
    //private ImageView uSrProImg;
    private String dscrptn="";
    private CircularImageView uSrProImg;
    private TextView txtDsgnation;
    private LinearLayout edtProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.user_profile_page);

        context = this;
        imgUsrProf = (ImageView) findViewById(R.id.imgProfll);
        notific = (ImageView) findViewById(R.id.imgNtfctnn);
        sttng = (ImageView) findViewById(R.id.imgSttngg);
        listSlctBtn = (ImageView) findViewById(R.id.imgLstBtn);
        adddEd = (TextView) findViewById(R.id.txtAdd);
        shaRedd = (TextView) findViewById(R.id.txtShrd);
        usErName = (TextView) findViewById(R.id.uSrName);
        gridBtn = (ImageView) findViewById(R.id.imgGridButtn);
        recyclerVw = (RecyclerView) findViewById(R.id.recyle_view);
        /*uSrProImg =(ImageView)findViewById(R.id.usRpro_img);*/
        uSrProImg=(CircularImageView)findViewById(R.id.usRpro_img);
        txtDsgnation=(TextView)findViewById(R.id.txtDsgntnt);
        edtProfile=(LinearLayout)findViewById(R.id.edtProfl);

        recyclerVw.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(context,1);
        recyclerVw.setLayoutManager(mLayoutManager);


        authorArray = Get_all_PostingEntity.getAuthor_data();
        postingData = Get_all_PostingEntity.getPosting_data();

        mAdapter = new GridRyciclerAdapter(context,postingData,"grid");
        recyclerVw.setAdapter(mAdapter);


        if (authorArray != null) {
            for (Get_all_PostingEntity gl : authorArray) {
                fullName = gl.getFull_name();
                proPic = gl.getProfile_pic();
                addEd = gl.getAdded();
                sharEd = gl.getShared();
                dscrptn = gl.getDescription();
            }
            adddEd.setText(addEd + " " + "Added");
            shaRedd.setText(sharEd + " " + "Shared");
            usErName.setText(fullName);
            txtDsgnation.setText(dscrptn);

            if (!proPic.equals("")){
                setProPic(proPic);
            }

        }

        gridBtn.setOnClickListener(this);
        listSlctBtn.setOnClickListener(this);
        edtProfile.setOnClickListener(this);




       /* imgUsrProf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserProfile_Activity.class);
                startActivity(intent);
                finish();
            }

        });*/
        notific.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Notification_Activity.class);
                startActivity(intent);
            }
        });
        sttng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SettingActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgGridButtn:

                mLayoutManager = new GridLayoutManager(context,2);
                recyclerVw.setLayoutManager(mLayoutManager);
                mAdapter = new GridRyciclerAdapter(context,postingData,"gridd");
                recyclerVw.setAdapter(mAdapter);

                break;

            case R.id.imgLstBtn:
                mLayoutManager = new GridLayoutManager(context,1);
                recyclerVw.setLayoutManager(mLayoutManager);
                mAdapter = new GridRyciclerAdapter(context,postingData,"grid");
                recyclerVw.setAdapter(mAdapter);
                break;
            case R.id.edtProfl:
                Intent intent = new Intent(context,Edit_ProfileActivity.class);
                startActivity(intent);
                break;
        }

    }

    public void setProPic(String url) {
        aQuery = new AQuery(context);

        try {

            aQuery.id(uSrProImg).image(url, true, true, 0, 0, new BitmapAjaxCallback() {

                @Override
                protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                    //super.callback(url, iv, bm, status);
                    iv.setImageBitmap(bm);
                }
            });

        } catch (NullPointerException e) {
            url = "";
            e.printStackTrace();

        }


    }
}
