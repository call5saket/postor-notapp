package com.example.saket.postornotapp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.saket.postornotapp.R;

/**
 * Created by Saket on 11/12/2015.
 */
public class PrivacyPolicyActivity extends AppCompatActivity {

    Context context;
    private ListView lstVw;
    private Toolbar toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_privacy_policy);

        context = this;
        toolBar =(Toolbar)findViewById(R.id.tollbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lstVw = (ListView) findViewById(R.id.lstPrcy);
        String[] values = new String[]{"Information we collect",
                "How we use information we collect",
                "Transparency and choice",
                "Information you share",
                "Information security",
                "When this privacy policy applies",
                "Changes",
                "Specific product practices"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        lstVw.setAdapter(adapter);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(context,MainActivity.class);
        startActivity(intent);

        finish();
        return true;
    }
}
