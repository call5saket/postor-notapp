package com.example.saket.postornotapp.sharedpref_db;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by mobiweb on 16/10/15.
 */
public class SharedPrfencesDb {

    public static final String Remember_me = "LoginState";
    public static final String PageUrl ="UserUrl";
    public static final String Login_key="rembrMe";
    public static final String Domain_key="usrUrl";
    public static final String UserId ="usrId";
    public static SharedPreferences settings;
    public Editor editor;

    public SharedPrfencesDb(Context context,String prefNm){
        super();
        settings = context.getSharedPreferences(prefNm,
                Context.MODE_PRIVATE);
    }

    public  void setLoginState( boolean state,String key){

      //  SharedPreferences pref = context.getSharedPreferences(Remember_me,Context.MODE_PRIVATE);
        //SharedPreferences.Editor prefEditor = pref.edit();
        editor =settings.edit();
        editor.putBoolean(key, state);
        editor.commit();

    }

    public  boolean getLoginState(Context context,String prefNm,String login_key){
        //SharedPreferences pref = context.getSharedPreferences(Remember_me,Context.MODE_PRIVATE);
        settings = context.getSharedPreferences(prefNm,
                Context.MODE_PRIVATE);

        return settings.getBoolean(login_key,false);
    }
    public  void setUsrPgUrl(String pg,String key){
        /*SharedPreferences pref = context.getSharedPreferences(PageUrl,Context.MODE_PRIVATE);
        SharedPreferences.Editor prEditor = pref.edit();*/
        editor =settings.edit();
        editor.putString(key,pg);
        editor.commit();

    }
    public  String getUsrUrl(Context context,String prefnm,String domain_key){
        //SharedPreferences pref =context.getSharedPreferences(PageUrl,Context.MODE_PRIVATE);
        settings = context.getSharedPreferences(prefnm,
                Context.MODE_PRIVATE);
        return settings.getString(domain_key,"");
    }

    public void removeFromSharedPreferences(Context context,String prefnm,String key){
        settings = context.getSharedPreferences(prefnm,Context.MODE_PRIVATE);


        if(settings!=null){
            settings.edit().remove(key).commit();
        }
    }

    public void saveUserId(String id,String key){

        editor = settings.edit();
        editor.putString(key,id);
        editor.commit();
    }

    public String getUsrId(Context context,String prefNm,String key){

        settings = context.getSharedPreferences(prefNm,Context.MODE_PRIVATE);

        return settings.getString(key,"");
    }
}
