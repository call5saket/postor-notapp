package com.example.saket.postornotapp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.saket.postornotapp.R;
import com.example.saket.postornotapp.entity.CreateUserEntity;
import com.example.saket.postornotapp.netcom.api.TaskCompleteListener;
import com.example.saket.postornotapp.netcome.CheckNetworkStateClass;
import com.example.saket.postornotapp.netcome.RequestResponseUpdtd;
import com.example.saket.postornotapp.sharedpref_db.SharedPrfencesDb;
import com.example.saket.postornotapp.utils.ConstantLib;

import java.util.ArrayList;

/**
 * Created by Saket on 11/12/2015.
 */
public class SignInActivity extends AppCompatActivity implements View.OnClickListener,TaskCompleteListener {

    Context context;
    private Toolbar toolBar;
    String userName ="";
    private TextView hdrTxt;
    private TextView frgtPsswrd;
    private EditText emailId;
    private EditText psswrd;
    private Button login;
   public  String suc ="";
    private String m_deviceId;
    private SharedPrfencesDb sp;
    private ArrayList<CreateUserEntity> val;
    private String usrDtl ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signin);
        context=this;

        toolBar =(Toolbar)findViewById(R.id.tollbar);
        hdrTxt =(TextView)findViewById(R.id.hadrtTxt);
        frgtPsswrd =(TextView)findViewById(R.id.frgtPsswrd);
        emailId =(EditText)findViewById(R.id.eMailId);
        psswrd =(EditText)findViewById(R.id.edtPswrd);
        login =(Button)findViewById(R.id.btnLogin);


        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TelephonyManager TelephonyMgr = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        m_deviceId = TelephonyMgr.getDeviceId();

        sp = new SharedPrfencesDb(context,SharedPrfencesDb.Remember_me);

        Bundle extras = getIntent().getExtras();


        if (extras != null) {
            userName = extras.getString("lgn");
            // and get whatever type user account id is
        }
        hdrTxt.setOnClickListener(this);
        frgtPsswrd.setOnClickListener(this);
        login.setOnClickListener(this);

    }

    private void saveCheckState() {
        sp.setLoginState(true,SharedPrfencesDb.Login_key);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (userName.equals("1")) {
            Intent intent = new Intent(context, SignUpActivity.class);
            startActivity(intent);

            finish();
        }else {
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);

            finish();
        }
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.hadrtTxt:
                Intent intent = new Intent(context,SignUpActivity.class);
                startActivity(intent);

                finish();
                break;

            case R.id.frgtPsswrd:
                Intent intent1 = new Intent(context,ForgetPasswordActivity.class);
                startActivity(intent1);
                break;

            case R.id.btnLogin:
                String  emAil = emailId.getText().toString();
                String passwrd = psswrd.getText().toString();

                if (emAil.equals("")) {
                    emailId.setError("Insert email");

                }else if(passwrd.equals("")){

                        psswrd.setError("Insert Password");

                }else if(!emAil.matches(emailMatch())){

            psswrd.setError("Insert Password");
        }else {
                    if (CheckNetworkStateClass.isOnline(context)) {

                        RequestResponseUpdtd.getInstance().performRequest(context, ConstantLib.BASE_URL+ConstantLib.URL_LOGIN,ConstantLib
                        .REQ_TYPE_LOGIN_TYPE,"POST",emAil,passwrd,"1",m_deviceId,"sgdfgsdfgsdfg356345fgsdfgsdfg");
                    } else {

                        Toast toast = Toast.makeText(context, "Check Network ", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }
                }



                break;
        }
    }

    public String emailMatch(){
        String email_match = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
                + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
                + "[a-zA-Z0-9][a-zA-Z0-9\\-]{1,3}" + ")+";

        return email_match;
    }

    @Override
    public void taskCompleted(int taskType, boolean status) {

        jsonLogin();
        switch (taskType){

            case ConstantLib.REQ_TYPE_LOGIN_TYPE:

                if(suc.equals("SUCCESS")){
                    Toast.makeText(context,suc,Toast.LENGTH_SHORT).show();

                    if (status){

                        if (CheckNetworkStateClass.isOnline(context)) {

                            RequestResponseUpdtd.getInstance().performRequest(context, ConstantLib.BASE_URL + ConstantLib.URL_GETAL_POSTINGS, ConstantLib
                                    .REQ_TYPE_GET_ALL_POSTINGS, "POST",usrDtl,"50","0",usrDtl);//usrDtl-- will be used as a first param
                        } else {

                            Toast toast = Toast.makeText(context, "Check Network ", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                        }

                    }
                }else {
                    Toast.makeText(context,suc,Toast.LENGTH_SHORT).show();
                }
                break;
            case ConstantLib.REQ_TYPE_GET_ALL_POSTINGS:
                if (status) {

                    saveCheckState();
                    Intent intent = new Intent(context, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
                break;
        }

    }

    public void jsonLogin() {
        if (CreateUserEntity.getValue() != null) {
            val = CreateUserEntity.getValue();
            System.out.println("size of valllllllllll list===========>" + val.size());

            for (CreateUserEntity log : val) {

                suc = log.getMessage();
                usrDtl = log.getUser_id();
                //msg = log.getSucc_msg();

            }
        }
    }
}
