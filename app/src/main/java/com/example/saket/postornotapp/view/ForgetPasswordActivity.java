package com.example.saket.postornotapp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.saket.postornotapp.R;
import com.example.saket.postornotapp.netcom.api.TaskCompleteListener;
import com.example.saket.postornotapp.netcome.CheckNetworkStateClass;
import com.example.saket.postornotapp.netcome.RequestResponseUpdtd;
import com.example.saket.postornotapp.utils.ConstantLib;

/**
 * Created by Saket on 11/12/2015.
 */
public class ForgetPasswordActivity extends AppCompatActivity implements TaskCompleteListener{

    Context context;
    private Button btnDne;
    private Toolbar toolBar;
    private EditText email;
    public static String suc ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.forgetpassword);

        context =this;

        toolBar =(Toolbar)findViewById(R.id.tolllbar);
        btnDne=(Button)findViewById(R.id.btnDn);
        email=(EditText)findViewById(R.id.edtFrgtPsswrd);


        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setIcon(R.mipmap.ic_post_or_not_logo_48dp);

        btnDne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

String eMaill = email.getText().toString();
                if(eMaill.equals("")){
                    email.setError("Insert Email");
                }else {

                    if (CheckNetworkStateClass.isOnline(context)) {

                        RequestResponseUpdtd.getInstance().performRequest(context, ConstantLib.BASE_URL + ConstantLib.URL_FORGTPASS, ConstantLib
                                .REQ_TYPE_FORGETPASS, "POST", eMaill);
                    } else {

                        Toast toast = Toast.makeText(context, "Check Network ", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }
                }

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(context,SignInActivity.class);
        startActivity(intent);

        finish();
        return true;
    }

    @Override
    public void taskCompleted(int taskType, boolean status) {

        switch (taskType){

            case ConstantLib.REQ_TYPE_FORGETPASS:
                if(suc.equals("SUCCESS")){
                    Intent intent = new Intent(context,ResetPasswordActivity.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(context,suc,Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
