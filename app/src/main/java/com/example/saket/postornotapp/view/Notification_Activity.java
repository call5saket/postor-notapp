package com.example.saket.postornotapp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.saket.postornotapp.R;

/**
 * Created by Saket on 15/12/2015.
 */
public class Notification_Activity extends AppCompatActivity {

    Context context;
    private ImageView imgUsrProf;
    private ImageView notific;
    private ImageView sttng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notification);

        context= this;


        imgUsrProf=(ImageView)findViewById(R.id.imgProflf);
        notific =(ImageView)findViewById(R.id.imgNtfctnf);
        sttng =(ImageView)findViewById(R.id.imgSttngf);

        imgUsrProf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserProfile_Activity.class);
                startActivity(intent);

            }

        });
        notific.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Notification_Activity.class);
                startActivity(intent);
                finish();
            }
        });
        sttng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,SettingActivity.class);
                startActivity(intent);
            }
        });
    }
}
