package com.example.saket.postornotapp.parser;

import com.andtinder.model.CardModel;
import com.example.saket.postornotapp.entity.Get_all_PostingEntity;
import com.example.saket.postornotapp.entity.UserDetailEntity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Saket on 12/01/2016.
 */
public class GetAllPosting_parser {

    private static GetAllPosting_parser instance;

    private boolean parseStatus = true;

    ArrayList<Get_all_PostingEntity>posting_author = new ArrayList<Get_all_PostingEntity>();
    ArrayList<Get_all_PostingEntity>posting_data = new ArrayList<Get_all_PostingEntity>();
    ArrayList<CardModel>cmdTnd= new ArrayList<CardModel>();

    public static GetAllPosting_parser getInstance() {

        if (instance == null) {

            instance = new GetAllPosting_parser();
        }
        return instance;
    }

    public boolean parseResponse(String jsonResponse) {
        parseStatus = true;
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(jsonResponse);

            Get_all_PostingEntity gtPNe = new Get_all_PostingEntity();

            gtPNe.setCode(jsonObj.getInt("code"));
            gtPNe.setMessage(jsonObj.getString("message"));

            JSONObject jsnResponObj = jsonObj.getJSONObject("response");
            JSONObject jsnPstng_athr__dta=jsnResponObj.getJSONObject("posting_author_data");
            JSONObject jsnPstng_dta = jsnResponObj.getJSONObject("posting_stats");
            JSONObject jsnDataInfo = jsnResponObj.getJSONObject("dataInfo");


            gtPNe.setUser_id(jsnPstng_athr__dta.getString("user_id"));
            gtPNe.setFacebook_id(jsnPstng_athr__dta.getString("facebook_id"));
            gtPNe.setGoogle_id(jsnPstng_athr__dta.getString("google_id"));
            gtPNe.setInstagram_access_token(jsnPstng_athr__dta.getString("instagram_access_token"));
            gtPNe.setFull_name(jsnPstng_athr__dta.getString("full_name"));
            gtPNe.setEmail(jsnPstng_athr__dta.getString("email"));
            gtPNe.setPassword(jsnPstng_athr__dta.getString("password"));
            gtPNe.setPassword_reset_key(jsnPstng_athr__dta.getString("password_reset_key"));
            gtPNe.setProfile_pic(jsnPstng_athr__dta.getString("profile_pic_url"));
            gtPNe.setGender(jsnPstng_athr__dta.getString("gender"));
            gtPNe.setDescription(jsnPstng_athr__dta.getString("description"));
            gtPNe.setPhone_number(jsnPstng_athr__dta.getString("phone_number"));
            gtPNe.setConnected_via(jsnPstng_athr__dta.getString("connected_via"));
            gtPNe.setUser_status(jsnPstng_athr__dta.getString("user_status"));
            gtPNe.setDevice_type(jsnPstng_athr__dta.getString("device_type"));
            gtPNe.setDevice_id(jsnPstng_athr__dta.getString("device_id"));
            gtPNe.setDevice_token(jsnPstng_athr__dta.getString("device_token"));
            gtPNe.setDate_created(jsnPstng_athr__dta.getString("date_created"));
            gtPNe.setAdded(jsnPstng_dta.getInt("added"));
            gtPNe.setShared(jsnPstng_dta.getInt("shared"));
            gtPNe.setCount(jsnDataInfo.getInt("count"));
            gtPNe.setHasNext(jsnDataInfo.getBoolean("hasNext"));

            posting_author.add(gtPNe);
            Get_all_PostingEntity.setInstance(gtPNe);
            Get_all_PostingEntity.setAuthor_data(posting_author);

            JSONArray jsnPstngDat = jsnResponObj.getJSONArray("posting_data");
            for (int i = 0; i<jsnPstngDat.length();i++){
                Get_all_PostingEntity gst = new Get_all_PostingEntity();

                CardModel crd = new CardModel();

                JSONObject jsnOb = jsnPstngDat.getJSONObject(i);

                gst.setPosting_id(jsnOb.getString("posting_id"));
                gst.setType(jsnOb.getString("type"));
                gst.setAttachment_url(jsnOb.getString("attachment_url"));
                gst.setLikes(jsnOb.getInt("likes"));
                gst.setDislikes(jsnOb.getInt("dislikes"));
                gst.setIs_like(jsnOb.getBoolean("is_like"));
                gst.setIs_dislike(jsnOb.getBoolean("is_dislike"));
                gst.setDate_created(jsnOb.getString("date_created"));
                crd.setImg(jsnOb.getString("attachment_url"));

                cmdTnd.add(crd);
                CardModel.setData(cmdTnd);

                posting_data.add(gst);

                Get_all_PostingEntity.setPosting_data(posting_data);
            }

        } catch (Exception e) {
            e.printStackTrace();
            parseStatus = false;

        }

return parseStatus;
    }
}