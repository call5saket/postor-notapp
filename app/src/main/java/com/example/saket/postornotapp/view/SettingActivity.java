package com.example.saket.postornotapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.saket.postornotapp.R;

/**
 * Created by Saket on 15/12/2015.
 */
public class SettingActivity extends AppCompatActivity {

    private Toolbar toolBaar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        toolBaar =(Toolbar)findViewById(R.id.tllb);
        setSupportActionBar(toolBaar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

      /*  if (userName.equals("1")) {
            Intent intent = new Intent(context, SignUpActivity.class);
            startActivity(intent);

            finish();
        }else {
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);

            finish();
        }*/
        return true;
    }
}
