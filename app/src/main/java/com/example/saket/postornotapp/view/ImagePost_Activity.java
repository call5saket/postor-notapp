package com.example.saket.postornotapp.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.saket.postornotapp.R;
import com.example.saket.postornotapp.netcom.api.TaskCompleteListener;
import com.example.saket.postornotapp.netcome.CheckNetworkStateClass;
import com.example.saket.postornotapp.netcome.RequestResponseUpdtd;
import com.example.saket.postornotapp.netcome.Request_MultyPart_Responce;
import com.example.saket.postornotapp.sharedpref_db.SharedPrfencesDb;
import com.example.saket.postornotapp.utils.ConstantLib;

/**
 * Created by Saket on 18/01/2016.
 */
public class ImagePost_Activity extends Activity implements TaskCompleteListener{

    Context context;
    private ImageButton crsBtn;
    private Button postBtn;
    private ImageView profileImg;
   static Uri bitmap;
    private SharedPrfencesDb sp;
    private String usrDtl ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_pic);

        context=this;

        crsBtn=(ImageButton)findViewById(R.id.crss_Btn);
        postBtn=(Button)findViewById(R.id.postImg);
        profileImg=(ImageView)findViewById(R.id.usrImg);

        profileImg.setImageURI(bitmap);

        sp = new SharedPrfencesDb(context,SharedPrfencesDb.Remember_me);

        usrDtl = sp.getUsrId(context,SharedPrfencesDb.Remember_me,SharedPrfencesDb.UserId);

        crsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        postBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetworkStateClass.isOnline(context)) {

                    Request_MultyPart_Responce.getInstance().performRequest(context, ConstantLib.BASE_URL + ConstantLib.URL_ADD_POSTING, ConstantLib
                            .REQ_TYPE_POST_IMAGES, "POSST",usrDtl,bitmap.toString(),"image","imgPost");//usrDtl-- will be used as a first param
                } else {

                    Toast toast = Toast.makeText(context, "Check Network ", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                }
            }
        });


    }

    @Override
    public void taskCompleted(int taskType, boolean status) {
        switch (taskType){
            case ConstantLib.REQ_TYPE_POST_IMAGES:

                if (status){

                    Toast.makeText(context,"Image Posted",Toast.LENGTH_LONG).show();
                    finish();
                }
        }

    }
    public ImagePost_Activity(Uri bm){
        bitmap = bm;
    }
    public ImagePost_Activity(){

    }
}
