package com.example.saket.postornotapp.netcom.api;

import android.content.Context;

public interface ResponseParseInterface {

	public boolean parseResponse(Context mContext, byte currentRequest, String jsonResponse);

}
