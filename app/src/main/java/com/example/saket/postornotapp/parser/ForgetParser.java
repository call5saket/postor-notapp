package com.example.saket.postornotapp.parser;

import com.example.saket.postornotapp.entity.ForgetPaswwrdEntity;
import com.example.saket.postornotapp.entity.LoginEntityBean;
import com.example.saket.postornotapp.view.ForgetPasswordActivity;
import com.example.saket.postornotapp.view.SignInActivity;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Saket on 19/12/2015.
 */
public class ForgetParser {

    private static ForgetParser instance;

    ArrayList<ForgetPaswwrdEntity>fgtG = new ArrayList<ForgetPaswwrdEntity>();

    private boolean parseStatus = true;

    public ForgetParser getInstance(){

        if (instance==null){

            instance = new ForgetParser();
        }

        return instance;
    }

    public boolean parseResponse(String jsonResponse){
        parseStatus = true;
        JSONObject jsonObj;
        try{
            jsonObj = new JSONObject(jsonResponse);

            ForgetPaswwrdEntity login=new ForgetPaswwrdEntity();

            login.setMessage(jsonObj.getString("message"));

            JSONObject jObj= jsonObj.getJSONObject("response");

            login.setPassword_reset_key(jObj.getString("password_reset_key"));

            ForgetPaswwrdEntity.setInstanse(login);




        }catch(Exception e){
            e.printStackTrace();
            parseStatus = false;
        }

        return parseStatus;

    }

}
