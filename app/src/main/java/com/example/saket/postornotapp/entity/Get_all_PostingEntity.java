package com.example.saket.postornotapp.entity;

import java.util.ArrayList;

/**
 * Created by Saket on 12/01/2016.
 */
public class Get_all_PostingEntity {

    private static Get_all_PostingEntity instance;

    public int code,likes,dislikes,added,shared,count;
    public boolean is_like,is_dislike,hasNext;
    public String message ="",posting_id ="",type ="",attachment_url="",date_created="",user_id="",facebook_id="",google_id="",instagram_access_token="",full_name="",email="",
            password="",password_reset_key="",profile_pic="",gender="",description="",phone_number="",connected_via="",user_status="",device_type="",device_id="",device_token="";

    public static ArrayList<Get_all_PostingEntity>author_data;
    public static ArrayList<Get_all_PostingEntity>posting_data;

    public static Get_all_PostingEntity getInstance() {

        if (instance==null){
            instance = new Get_all_PostingEntity();
        }
        return instance;
    }

    public static void setInstance(Get_all_PostingEntity instance) {
        Get_all_PostingEntity.instance = instance;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static ArrayList<Get_all_PostingEntity> getAuthor_data() {
        return author_data;
    }

    public static void setAuthor_data(ArrayList<Get_all_PostingEntity> author_data) {
        Get_all_PostingEntity.author_data = author_data;
    }

    public static ArrayList<Get_all_PostingEntity> getPosting_data() {
        return posting_data;
    }

    public static void setPosting_data(ArrayList<Get_all_PostingEntity> posting_data) {
        Get_all_PostingEntity.posting_data = posting_data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return dislikes;
    }

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

    public int getAdded() {
        return added;
    }

    public void setAdded(int added) {
        this.added = added;
    }

    public int getShared() {
        return shared;
    }

    public void setShared(int shared) {
        this.shared = shared;
    }

    public boolean is_like() {
        return is_like;
    }

    public void setIs_like(boolean is_like) {
        this.is_like = is_like;
    }

    public boolean is_dislike() {
        return is_dislike;
    }

    public void setIs_dislike(boolean is_dislike) {
        this.is_dislike = is_dislike;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPosting_id() {
        return posting_id;
    }

    public void setPosting_id(String posting_id) {
        this.posting_id = posting_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAttachment_url() {
        return attachment_url;
    }

    public void setAttachment_url(String attachment_url) {
        this.attachment_url = attachment_url;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getGoogle_id() {
        return google_id;
    }

    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    public String getInstagram_access_token() {
        return instagram_access_token;
    }

    public void setInstagram_access_token(String instagram_access_token) {
        this.instagram_access_token = instagram_access_token;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_reset_key() {
        return password_reset_key;
    }

    public void setPassword_reset_key(String password_reset_key) {
        this.password_reset_key = password_reset_key;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getConnected_via() {
        return connected_via;
    }

    public void setConnected_via(String connected_via) {
        this.connected_via = connected_via;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }
}
