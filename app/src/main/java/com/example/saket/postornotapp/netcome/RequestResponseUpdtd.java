package com.example.saket.postornotapp.netcome;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;


import com.example.saket.postornotapp.netcom.api.TaskCompleteListener;
import com.example.saket.postornotapp.utils.ConstantLib;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

/**
 * Created by mobiweb on 21/9/15.
 */
public class RequestResponseUpdtd {

    private static Fragment mcontext = null;
    private static Context mContext = null;
    private static Context context = null;
    private static byte currentRequest = -1;
    private static String url;
    // private String[] values;
    private static RequestResponseUpdtd instance;
    private static String method =" ";

    private static boolean networkFlag = false, no_peer_certificate = false;
    private static boolean parseStatus = false;
    private static ProgressDialog progressDialog = null;
    public static String str_response = "";
   public static String sb_error ="";

    public static RequestResponseUpdtd getInstance(){

        if(null == instance){

            instance = new RequestResponseUpdtd();
        }
        return instance;
    }
    public void performRequest(Context context, String url,
                               byte currentRequest,String method, String... params) {
        RequestResponseUpdtd.currentRequest = -1;
        RequestResponseUpdtd.context = context;
        RequestResponseUpdtd.currentRequest = currentRequest;
        RequestResponseUpdtd.url = url;
        RequestResponseUpdtd.method=method;

        // values = params;
        if (true) {

            ServerCommunication.getInstance().execute(params);
            // }

        }
    }
    public void performRequestt(Context contextt,Fragment context, String url,
                                byte currentRequest, String method, String... params) {
        RequestResponseUpdtd.currentRequest = -1;
        RequestResponseUpdtd.mContext = contextt;
        RequestResponseUpdtd.mcontext = context;
        RequestResponseUpdtd.currentRequest = currentRequest;
        RequestResponseUpdtd.url = url;
        RequestResponseUpdtd.method = method;

        // values = params;
        if (true) {

            ServerCommunication.getInstance().execute(params);
            // }

        }
    }



    public static Handler handler = new Handler() {

        @Override
        public void handleMessage(android.os.Message msg) {
            int taskType = msg.what;

            if (progressDialog != null)
                if (progressDialog.isShowing()) {
                    try {
                        progressDialog.cancel();
                        progressDialog = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            if (context != null) {
                TaskCompleteListener taskCompleteListener = (TaskCompleteListener) context;
                taskCompleteListener.taskCompleted(currentRequest, true);
            } else {
                TaskCompleteListener taskCompleteListener = (TaskCompleteListener) mcontext;
                taskCompleteListener.taskCompleted(currentRequest, true);
            }
        };

    };


    private static class ServerCommunication extends
            AsyncTask<String, String, String> {

        private static ServerCommunication instance;
        private HttpURLConnection urlConnection;


        public static ServerCommunication getInstance() {
            // if (null == instance) {
            instance = new ServerCommunication();
            // }
            return instance;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (context!=null) {
                    progressDialog = new ProgressDialog(context);
                    //progressDialog = MyCustomProgressDialog.ctor(context);
                    //String msg = context.getString(R.string.prgssWheel);
                    progressDialog.setMessage("Please wait");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                }else {
                    progressDialog = new ProgressDialog(mContext);
                    //progressDialog = MyCustomProgressDialog.ctor(context);
                    //String msg = mContext.getString(R.string.prgssWheel);
                    progressDialog.setMessage("Please wait");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(String responseJson) {
            super.onPostExecute(responseJson);

            long start = System.currentTimeMillis();
            if (networkFlag) {
                if (responseJson != null) {
                    if (responseJson.length() > 0) {
                        if (context!=null){
                            parseStatus = ResponseParserFactory.getInstance()
                                    .parseResponse(context, currentRequest,
                                            responseJson);
                            responseJson = null;
                        }else {
                            parseStatus = ResponseParserFactory.getInstance()
                                    .parseResponse(mContext, currentRequest,
                                            responseJson);
                            responseJson = null;
                        }
                    }
                }
            } else if (no_peer_certificate) {

                // Toast.makeText(context, R.string.no_peer, 1).show();
            } else {

                if (context!=null){
                    Toast.makeText(context, "network error", Toast.LENGTH_SHORT)
                            .show();
                }else {
                    Toast.makeText(mContext, "network error", Toast.LENGTH_SHORT)
                            .show();
                }
                sb_error = "network error";
            }

            // Log.v(ConstantLib.LOG, "Server Response : " + responseJson);
            long end = System.currentTimeMillis();
            long diff = end - start;

            Log.e("Post Eecute", "ooooo" + diff);
            handler.sendEmptyMessage(currentRequest);
        }



        @Override
        protected String doInBackground(String... params) {

            URL urll;
            networkFlag = true;
            String responseJson = " ";
            String query ="";
            String charset = "UTF-8";

            if("POST".equals(method)){
                if (networkFlag) {

                    try {
                        urll = new URL(url);
                        System.out.println("Final url---------------"+url);
                        HttpURLConnection conn = (HttpURLConnection) urll.openConnection();
                        conn.setReadTimeout(ConstantLib.READTIMEOUT);
                        conn.setConnectTimeout(ConstantLib.CONNECTION_TIMEOUT);
                        conn.setRequestMethod("POST");
                        conn.setDoInput(true);
                        conn.setDoOutput(true);

                        OutputStream os = conn.getOutputStream();
                        BufferedWriter writer = new BufferedWriter(
                                new OutputStreamWriter(os, "UTF-8"));

                        HashMap<String, String> postDataParams = new HashMap<String,String>();

                        switch (currentRequest) {

                            case ConstantLib.REQ_TYPE_LOGIN_TYPE:
                                postDataParams.put("email",params[0]);
                                postDataParams.put("password",params[1]);
                                postDataParams.put("device_type",params[2]);
                                postDataParams.put("device_id",params[3]);
                                postDataParams.put("device_token",params[4]);


                                break;

                            case ConstantLib.REQ_TYPE_SIGN_UP:

                                postDataParams.put("fullname",params[0]);
                                postDataParams.put("nameoftype",params[1]);
                                postDataParams.put("selectcountry",params[2]);
                                postDataParams.put("areauser",params[3]);
                                postDataParams.put("cityuser",params[4]);
                                postDataParams.put("streetuser",params[5]);
                                postDataParams.put("chooseyournamepage",params[6]);
                                postDataParams.put("chooseusername",params[7]);
                                postDataParams.put("passworduser",params[8]);
                                postDataParams.put("repassworduser",params[9]);
                                postDataParams.put("emailuser",params[10]);
                                postDataParams.put("mobile",params[11]);
                                postDataParams.put("selectcategory",params[12]);
                                postDataParams.put("gender",params[13]);
                                postDataParams.put("bday",params[14]);
                                postDataParams.put("logo",params[15]);


                                break;
                            case ConstantLib.REQ_TYPE_FORGETPASS:
                                postDataParams.put("email",params[0]);

                                break;

                         /*   case ConstantLib.REQ_TYPE_SEARCHELECTN:
                                postDataParams.put("searchkey",params[0]);
                                postDataParams.put("domain",params[1]);
                                break;*/
                            case ConstantLib.REQ_TYP_GET_USRDETAIL:

                                postDataParams.put("user_id",params[0]);

                                break;
                            case ConstantLib.REQ_TYPE_GET_ALL_POSTINGS:

                                postDataParams.put("user_id",params[0]);
                                postDataParams.put("limit",params[1]);
                                postDataParams.put("offset",params[2]);
                                postDataParams.put("logged_in_user_id",params[3]);

                                break;


                        }

                        writer.write(getPostDataString(postDataParams));
                        System.out.println("writerrrrrr urllll"+writer.toString());

                        writer.flush();
                        writer.close();

                        os.close();

                        int responseCode=conn.getResponseCode();

                        System.out.println("requesttttttttttttttttt codeeeeeeeeeee===========>"+responseCode);

                        if (responseCode == HttpsURLConnection.HTTP_OK) {
                            String line;
                            BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            while ((line=br.readLine()) != null) {
                                responseJson+=line;

                                System.out.println("Responceeeeeeeeeeee"+line);
                            }
                        }
                        else {
                            responseJson="";

                            //throw new HttpException(responseCode+"");
                            System.out.println("Errorrrrrrrrrrrrrrrrrrrrrrrrrrrr ");
                        }

                    } catch (ConnectTimeoutException e) {

                        networkFlag = false;
                        e.printStackTrace();
                    } catch (SocketTimeoutException e) {

                        networkFlag = false;
                        e.printStackTrace();
                    } catch (SSLPeerUnverifiedException ex) {
                        // TODO: handle exception
                        networkFlag = false;
                        no_peer_certificate = true;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else {
                    responseJson = null;
                }

            }else {
                // for Get method
                if (networkFlag) {



                    switch (currentRequest) {

                        case ConstantLib.REQ_TYPE_LOGIN_TYPE:
                            //postDataParams.put("login_key",params[0]);

                            try {
                                query = String.format("username=%s&password=%s",
                                        URLEncoder.encode(params[0], charset),
                                        URLEncoder.encode(params[1], charset));

                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                            break;

                       /* case ConstantLib.REQ_TYPE_SEARCHELECTN:
                            try {
                                query = String.format("searchkey=%s&domain=%s",
                                        URLEncoder.encode(params[0], charset),
                                        URLEncoder.encode(params[1], charset));

                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }*/










                    }
                    try {
                        urll = new URL(url+query);

                        System.out.println("urlllllllllllllllllllllll"+urll.toString());

                        urlConnection = (HttpURLConnection) urll.openConnection();
                        int responseCode = urlConnection.getResponseCode();

                        if (responseCode == HttpsURLConnection.HTTP_OK) {
                            String responseString = readStream(urlConnection.getInputStream());
                            Log.v("CatalogClient", responseString);
                            // response = new JSONArray(responseString);
                            responseJson = responseString;
                        } else {
                            Log.v("CatalogClient", "Response code:" + responseCode);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (urlConnection != null)
                            urlConnection.disconnect();
                    }
                }
            }


            return responseJson;
        }

        private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;
            for(Map.Entry<String, String> entry : params.entrySet()){
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }

            System.out.println("Urllllllllllll" + result.toString());

            return result.toString();
        }

        private String readStream(InputStream in) {
            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }
}
