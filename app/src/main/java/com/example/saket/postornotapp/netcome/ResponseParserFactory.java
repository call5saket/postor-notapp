package com.example.saket.postornotapp.netcome;

import android.content.Context;
import android.util.Log;


import com.example.saket.postornotapp.entity.CreateUserEntity;
import com.example.saket.postornotapp.entity.Get_all_PostingEntity;
import com.example.saket.postornotapp.entity.LoginEntityBean;
import com.example.saket.postornotapp.entity.UserDetailEntity;
import com.example.saket.postornotapp.netcom.api.ResponseParseInterface;

import com.example.saket.postornotapp.parser.CreateUserParser;
import com.example.saket.postornotapp.parser.GetAllPosting_parser;
import com.example.saket.postornotapp.parser.LoginParse;
import com.example.saket.postornotapp.parser.UserDtlParser;
import com.example.saket.postornotapp.utils.ConstantLib;


public class ResponseParserFactory implements ResponseParseInterface {

	private static ResponseParserFactory instance;
	private boolean parseStatus = false;
	private Context mContext;
	private String sessionResult = "";

	private ResponseParserFactory() {
	}

	public static ResponseParserFactory getInstance() {
		if (null == instance) {
			instance = new ResponseParserFactory();
		}
		return instance;
	}
    
	public boolean parseResponse(Context mContext, byte currentRequest,
			String jsonResponse) {
		// TODO Auto-generated method stub
		this.mContext = mContext;
		try {
			sessionResult = "";
			switch (currentRequest) {
			case ConstantLib.REQ_TYPE_LOGIN_TYPE:


				CreateUserEntity.setInstance(null);
				parseStatus = CreateUserParser.getInstance().parseResponse(jsonResponse);

				/*LoginEntityBean.setInstance(null);

				parseStatus = LoginParse.getInstance().parseResponse(
						jsonResponse);*/

				break;

				case ConstantLib.REQ_TYPE_SIGN_UP:

					CreateUserEntity.setInstance(null);

					parseStatus = CreateUserParser.getInstance().parseResponse(
							jsonResponse);

					break;

				case ConstantLib.REQ_TYPE_FORGETPASS:
					LoginEntityBean.setInstance(null);

					parseStatus = LoginParse.getInstance().parseResponse(
							jsonResponse);

					break;
				case ConstantLib.REQ_TYP_GET_USRDETAIL:
					UserDetailEntity.setInstance(null);
					parseStatus = UserDtlParser.getInstance().parseResponse(jsonResponse);

					break;
				case ConstantLib.REQ_TYPE_GET_ALL_POSTINGS:
if (Get_all_PostingEntity.author_data!=null&&Get_all_PostingEntity.posting_data!=null){
					Get_all_PostingEntity.author_data.clear();
					Get_all_PostingEntity.posting_data.clear();}
					Get_all_PostingEntity.setInstance(null);

					parseStatus = GetAllPosting_parser.getInstance().parseResponse(jsonResponse);

					break;
				case ConstantLib.REQ_TYPE_POST_IMAGES:
					LoginEntityBean.setInstance(null);

					parseStatus = LoginParse.getInstance().parseResponse(
							jsonResponse);

					break;
				/*case ConstantLib.REQ_TYPE_SEARCHELECTN:
					User_Interection_SrchEntity.setInstance(null);

					parseStatus = UserSerchHome_parser.getInstance().parseResponse(jsonResponse);

					break;*/
           /* case ConstantLib.REQ_TYPE_SPLASH:
				
				Splash_Entity.setInstance(null);
				
				//HomePageEntityBean.getPry().clear();
				
				parseStatus = Splash_Parser.getInstance().parseResponse(jsonResponse);
						
			break;
			
			case ConstantLib.REQ_TYPE_HOMEPAGE:
				
				HomePageEntityBean.setInstance(null);
				
				//HomePageEntityBean.getPry().clear();
				
				parseStatus = HomePageParser.getInstance().parseResponse(jsonResponse);
						
			break;
			
			case ConstantLib.REQ_TYPE_GETSLIDER:
				
				HomePageSliderEntityBean.setInstance(null);
				//HomePageSliderEntityBean.getHpeb().clear();
				
				parseStatus = HomePageSliderParser.getInstance().parseResponse(jsonResponse);
				
			break;	
			
			case ConstantLib.REQ_TYPE_ABOUTUS:
				About_usEntityBean.setInstance(null);
				
				parseStatus = About_usParser.getInstance().parseResponse(jsonResponse); 
            
			break;
			
			case ConstantLib.REQ_TYPE_HELP_US:
				
				Helpus_Donate_Entity_Bean.setInstance(null);
				//Helpus_Donate_Entity_Bean.getPrii().clear();
				
				
				parseStatus = Help_us_Donate_Parser.getInstance().parseResponse(jsonResponse);
				
			break;	
			
			case ConstantLib.REQ_TYPE_LABOUR_DAY:
				About_usEntityBean.setInstance(null);
				
				parseStatus = About_usParser.getInstance().parseResponse(jsonResponse);
				
			break;
			
			case ConstantLib.REQ_TYPE_CUSTOM_ABOUT_US:
				About_usEntityBean.setInstance(null);
				
				parseStatus = About_usParser.getInstance().parseResponse(jsonResponse);
				
				break;
            
			case ConstantLib.REQ_TYPE_HEAT_PUMP_WATER_HEATER:
                About_usEntityBean.setInstance(null);
				
				parseStatus = About_usParser.getInstance().parseResponse(jsonResponse);
               
				break;
				
			case ConstantLib.REQ_TYPE_CUSTOM_1:
				
                About_usEntityBean.setInstance(null);
				
				parseStatus = Custom1_Parser.getInstance().parseResponse(jsonResponse);

			
				break;
			case ConstantLib.REQ_TYPE_CUSTOM_2:
				
                About_usEntityBean.setInstance(null);
				
				parseStatus = About_usParser.getInstance().parseResponse(jsonResponse);
				
				break;
				
			case ConstantLib.REQ_TYPE_SEARCH:
				
				Search_ProductEntity.setInstance(null);
				
				parseStatus = Search_product_parser.getInstance().parseResponse(jsonResponse);
				
				break;
				
			case ConstantLib.REQ_TYPE_PRODUCT_DETAILLIST:
				
				Product_detailList_entity.setInstance(null);
				
				parseStatus = Product_DetailParser.getInstance().parseResponse(jsonResponse); 
                
				break;
				
			case ConstantLib.REQ_TYPE_LOCATION_ON_MAP:
				
				Location_OnMap_Entity.setInstance(null);
				
				parseStatus = LocationOn_MapParser.getInstance().parseResponse(jsonResponse);
				
				break;
			case ConstantLib.REQ_TYPE_LOGIN_TYPE:
				
				
           	 LoginEntityBean.setInstance(null);
				parseStatus = Login_Parser.getInstance().parseResponse(jsonResponse); 
               
				break;
            case ConstantLib.REQ_TYPE_REG_TYPE:
				
				
           	     LoginEntityBean.setInstance(null);
				parseStatus = CreateAccount_Parser.getInstance().parseResponse(jsonResponse); 
                
				break;
					
            case ConstantLib.REQ_TYPE_FROGET_TYPE:
 				
 				
           	 LoginEntityBean.setInstance(null);
				parseStatus = ForgetPassword_Parser.getInstance().parseResponse(jsonResponse); 
                
				break;		
				
            case ConstantLib.REQ_TYPE_SHIPPING_METHOD:
            	
            	Shipping_MethodEntity_Bean.setInstance(null);
            	
            	parseStatus = Shipping_MethodParser.getInstance().parseResponse(jsonResponse);
            	
            	break;
            case ConstantLib.REQ_TYPE_CONTECT_US:

				LoginEntityBean.setInstance(null);
				parseStatus = Contect_Us_Parser.getInstance().parseResponse(
						jsonResponse);

				break;
            case ConstantLib.REQ_TYPE_PASSWORD_UPDATE:

            	ChangePassword_Entity.setInstance(null);
				parseStatus = ChangePassword_Parser.getInstance().parseResponse(
						jsonResponse);
				break;	
           case ConstantLib.REQ_TYPE_SEARCH_PRODUCT:

        	    Product_detailList_entity.setInstance(null);
        	    parseStatus = SearchList_Parser.getInstance().parseResponse(jsonResponse); 

 				break;
            case ConstantLib.REQ_TYPE_SEARCH_SCROLL:
			
			Search_ProductEntity.setInstance(null);
			
			parseStatus = Search_product_parser.getInstance().parseResponse(jsonResponse);
			
			break;
			
            case ConstantLib.REQ_TYPE_EXISTING_ADDRESS:
            	
            	Existing_Address_Entity.setInstance(null);
            	
            	parseStatus = Existing_Address_Parser.getInstance().parseResponse(jsonResponse);
            break;
            
            case ConstantLib.REQ_TYPE_SHIPPING_ADDRESS:
            	
            	Shipping_AddressEntity.setInstance(null);
            	
            	parseStatus = ShippingAddrss_Parser.getInstanse().parseResponse(jsonResponse);
            	
            	break;
            case ConstantLib.REQ_TYPE_UPDATE_ADDRESS:
            	
            	UpdateShippingAddrs_Entity.setInstance(null);
            	
            	parseStatus = UpdateShipping_AddrssParser.getInstance().parseResponse(jsonResponse);
            	
            	
            	break;
            case ConstantLib.REQ_TYPE_COUPON:
            	Coupons_SpecialEntity.setInstance(null);
            	
            	parseStatus =Coupons_Special_Parser.getInstance().parseResponse(jsonResponse);
            	
            	break;
            case ConstantLib.REQ_TYPE_STATES:
            	States_entity.setInstance(null);
            	
            	parseStatus =States_parser.getInstance().parseResponse(jsonResponse);
            	
            	break;
            case ConstantLib.REQ_TYPE_SHIPPING_CHARGES:
            	 Shipping_AddressEntity.setInstance(null);
            	 
            	 parseStatus = Shipping_charge_parser.getInstance().parseResponse(jsonResponse);
            	
            	
            	break;*/
			}
					

			// handler.sendEmptyMessage(currentRequest);

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(ConstantLib.LOG, e.getLocalizedMessage(), e);
		}
		return parseStatus;
		
	}
	

	/*private final Handler handler = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {
			int taskType = msg.what;

			Iterator<TaskCompleteListener> iterator = ConstantLib.TASK_COMPLETE_SET
					.iterator();

			while (iterator.hasNext()) {
				TaskCompleteListener taskCompleteListener = iterator.next();
				taskCompleteListener.taskCompleted(taskType, parseStatus);

			}

		};

	};*/

	

}
