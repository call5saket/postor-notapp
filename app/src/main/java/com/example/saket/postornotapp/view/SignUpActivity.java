package com.example.saket.postornotapp.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.saket.postornotapp.R;
import com.example.saket.postornotapp.entity.CreateUserEntity;
import com.example.saket.postornotapp.netcom.api.TaskCompleteListener;
import com.example.saket.postornotapp.netcome.CheckNetworkStateClass;
import com.example.saket.postornotapp.netcome.Request_MultyPart_Responce;
import com.example.saket.postornotapp.utils.ConstantLib;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Saket on 11/12/2015.
 */
public class SignUpActivity extends AppCompatActivity implements View.OnClickListener,TaskCompleteListener {

    Context context;
    private Toolbar toolBar;
    private TextView headrText;
    private ImageView usrPro;
    private EditText usrName;
    private String selectedImagePath ="";
    private static final int REQUEST_CAMERA = 100;
    private static final int SELECT_FILE = 200;
    private Intent dataa;
    private EditText eMail;
    private EditText psswrd;
    private Button btnSignUp;
    private String m_deviceId ="";
    private ArrayList<CreateUserEntity> val;
    private String succ ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);
        context = this;

        headrText = (TextView) findViewById(R.id.hadrTxt);
        toolBar = (Toolbar) findViewById(R.id.tollbar);
        usrPro = (ImageView) findViewById(R.id.ImgUsrPro);
        usrName = (EditText) findViewById(R.id.edtUsrNm);
        eMail=(EditText)findViewById(R.id.edtEml);
        psswrd=(EditText)findViewById(R.id.edtPswrd);
        btnSignUp=(Button)findViewById(R.id.btnSign);

        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Device Id---

        TelephonyManager TelephonyMgr = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
         m_deviceId = TelephonyMgr.getDeviceId();

        headrText.setOnClickListener(this);
        usrPro.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);

        finish();
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.hadrTxt:

                Intent intent = new Intent(context, SignInActivity.class);
                intent.putExtra("lgn", "1");
                startActivity(intent);
                finish();

                break;
            case R.id.ImgUsrPro:
                selectImage();
                break;

            case R.id.btnSign:
                String userName = usrName.getText().toString();
                String pssswrd =  psswrd.getText().toString();
                String mailE   = eMail.getText().toString();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (userName.equals("")){
                    usrName.setError("Insert full name");
                }else if (pssswrd.equals("")){
                    psswrd.setError("Insert password");
                }else if(mailE.equals("")){
                    eMail.setError("Insert email");
                }else if (!mailE.matches(emailPattern)){
                     eMail.setError("Insert correct format email");
                }
                else {
                    if (CheckNetworkStateClass.isOnline(context)) {

                        Request_MultyPart_Responce.getInstance().performRequest(context, ConstantLib.BASE_URL+ConstantLib.URL_REG,ConstantLib.REQ_TYPE_SIGN_UP,"POST",userName,""
                        ,"","","normal","1",m_deviceId,"zxcvzcvzxcv451234vcsdfsdvsdcd",pssswrd,mailE,selectedImagePath);


                        //jsonLogin();
                    } else {

                        Toast toast = Toast.makeText(context, "Check Network ", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }
                }


                break;
        }

    }

    public String emailMatch(){
        String email_match = "[a-zA-Z0-9]+@[a-z]+\\.+[a-z]+";

        return email_match;
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");

                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            dataa = data;
            if (requestCode == REQUEST_CAMERA) {

                selectedImagePath = getImagePath();
                usrPro.setImageBitmap(decodeFile(selectedImagePath));

                // imageView.setImageBitmap(bitmapImage );

                // dataa= (Uri)data.getExtras().get("data");

              /*  Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();



                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/

                // img_pro.setImageBitmap(bitmapImage);

            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null,
                        null);
                cursor.moveToFirst();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                //cursor.moveToFirst();

                selectedImagePath = cursor.getString(column_index);
                cursor.close();
                System.out.println("Select path===============>" + selectedImagePath.toString());

                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);

                usrPro.setImageBitmap(bm);
            }
        }
    }
    //decode for the camera

    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 90;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }

    // geting URi for the  camera save image
    public Uri setImageUri() {
        // Store image in dcim
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".Jpg");
        Uri imgUri = Uri.fromFile(file);
        selectedImagePath = file.getAbsolutePath();
        return imgUri;
    }

    public String getImagePath() {
        return selectedImagePath;
    }

    @Override
    public void taskCompleted(int taskType, boolean status) {
        jsonLogin();
    switch (taskType){
        case ConstantLib.REQ_TYPE_SIGN_UP:
            if(succ.equals("SUCCESS")){
                Toast.makeText(context,succ,Toast.LENGTH_SHORT).show();

                if (status){
                    Intent intent = new Intent(context,SignInActivity.class);
                    startActivity(intent);
                }
            }else {
                Toast.makeText(context,succ,Toast.LENGTH_SHORT).show();
            }

            break;


    }
    }

    public void jsonLogin() {
        if (CreateUserEntity.getValue() != null) {
            val = CreateUserEntity.getValue();
            System.out.println("size of valllllllllll list===========>" + val.size());

            for (CreateUserEntity log : val) {

                succ = log.getMessage();
                //msg = log.getSucc_msg();

            }
        }
    }

}


