package com.example.saket.postornotapp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.saket.postornotapp.R;
import com.example.saket.postornotapp.netcom.api.TaskCompleteListener;
import com.example.saket.postornotapp.netcome.CheckNetworkStateClass;
import com.example.saket.postornotapp.netcome.RequestResponseUpdtd;
import com.example.saket.postornotapp.sharedpref_db.SharedPrfencesDb;
import com.example.saket.postornotapp.utils.ConstantLib;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,TaskCompleteListener {
    Context context;
    private TextView txtSgnInn;
    String htmlString = "<u>Sign in</u>";
    private Button btnJoin;
    private TextView txtPrivcy;
    private SharedPrfencesDb sp;
    private String usrDtl="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        txtSgnInn = (TextView) findViewById(R.id.txtSgnIn);
        txtSgnInn.setText(Html.fromHtml(htmlString));
        txtPrivcy=(TextView)findViewById(R.id.txtPrvcy);

        btnJoin = (Button) findViewById(R.id.btnJoiNw);

        sp = new SharedPrfencesDb(context,SharedPrfencesDb.Remember_me);

        if(sp.getLoginState(context,SharedPrfencesDb.Remember_me,SharedPrfencesDb.Login_key)){

            if (CheckNetworkStateClass.isOnline(context)) {
                usrDtl = sp.getUsrId(context,SharedPrfencesDb.Remember_me,SharedPrfencesDb.UserId);

                RequestResponseUpdtd.getInstance().performRequest(context, ConstantLib.BASE_URL + ConstantLib.URL_GETAL_POSTINGS, ConstantLib
                        .REQ_TYPE_GET_ALL_POSTINGS, "POST",usrDtl,"50","0",usrDtl);//usrDtl-- will be used as a first param
            } else {

                Toast toast = Toast.makeText(context, "Check Network ", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

            }



        }else {
          /*  Intent intent = new Intent(getApplicationContext(), UserIntrection_Activity.class);
            startActivity(intent);
            finish();*/
        }


        btnJoin.setOnClickListener(this);
        txtSgnInn.setOnClickListener(this);
        txtPrivcy.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.txtSgnIn:

                Intent intent = new Intent(context, SignInActivity.class);
                startActivity(intent);
                finish();
                break;


            case R.id.btnJoiNw:

                Intent intent1 = new Intent(context, SignUpActivity.class);

                startActivity(intent1);
                finish();
                break;

            case R.id.txtPrvcy:
                Intent intent2 = new Intent(context,PrivacyPolicyActivity.class);
                startActivity(intent2);
                break;
        }

    }

    @Override
    public void taskCompleted(int taskType, boolean status) {

        switch (taskType){
            case ConstantLib.REQ_TYPE_GET_ALL_POSTINGS:
                if (status){
                    Intent intent = new Intent(context,HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
        }

    }
}
