package com.example.saket.postornotapp.entity;

import java.util.ArrayList;

/**
 * Created by mobiweb on 29/9/15.
 */
public class LoginEntityBean {

    private static LoginEntityBean instance;

    public String  success = "",succ_msg = "";



    private static ArrayList<LoginEntityBean> value;

    public static LoginEntityBean getInstance() {
        if(null == instance){
            instance = new LoginEntityBean();
        }
        return instance;
    }

    public static void setInstance(LoginEntityBean instance) {
        LoginEntityBean.instance = instance;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSucc_msg() {
        return succ_msg;
    }

    public void setSucc_msg(String succ_msg) {
        this.succ_msg = succ_msg;
    }

    public static ArrayList<LoginEntityBean> getValue() {
        return value;
    }

    public static void setValue(ArrayList<LoginEntityBean> value) {
        LoginEntityBean.value = value;
    }
}
