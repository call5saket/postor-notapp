package com.example.saket.postornotapp.netcome;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.example.saket.postornotapp.netcom.api.TaskCompleteListener;
import com.example.saket.postornotapp.utils.ConstantLib;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

/**
 * Created by mobiweb on 5/10/15.
 */
public class Request_MultyPart_Responce {

    private static Context context = null;
    private static Fragment mcontext = null;
    private static Context mContext = null;
    private static byte currentRequest = -1;
    private static String url;
    // private String[] values;
    private static Request_MultyPart_Responce instance;
    private static String method = " ";

    private static boolean networkFlag = false, no_peer_certificate = false;
    private static boolean parseStatus = false;
    private static ProgressDialog progressDialog = null;
    public static String str_response = "";
    public static String sb_error = "";

    public static Request_MultyPart_Responce getInstance() {

        if (null == instance) {

            instance = new Request_MultyPart_Responce();
        }
        return instance;
    }

    public void performRequest(Context context, String url,
                               byte currentRequest, String method, String... params) {
        Request_MultyPart_Responce.currentRequest = -1;
        Request_MultyPart_Responce.context = context;
        Request_MultyPart_Responce.currentRequest = currentRequest;
        Request_MultyPart_Responce.url = url;
        Request_MultyPart_Responce.method = method;

        // values = params;
        if (true) {

            ServerCommunication.getInstance().execute(params);
            // }

        }
    }

    public void performRequestt(Context contextt,Fragment context, String url,
                                byte currentRequest, String method, String... params) {
        Request_MultyPart_Responce.currentRequest = -1;
        Request_MultyPart_Responce.mContext = contextt;
        Request_MultyPart_Responce.mcontext = context;
        Request_MultyPart_Responce.currentRequest = currentRequest;
        Request_MultyPart_Responce.url = url;
        Request_MultyPart_Responce.method = method;

        // values = params;
        if (true) {

            ServerCommunication.getInstance().execute(params);
            // }

        }
    }


    public static Handler handler = new Handler() {

        @Override
        public void handleMessage(android.os.Message msg) {
            int taskType = msg.what;

            if (progressDialog != null)
                if (progressDialog.isShowing()) {
                    try {
                        progressDialog.cancel();
                        progressDialog = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            if (context != null) {
                TaskCompleteListener taskCompleteListener = (TaskCompleteListener) context;
                taskCompleteListener.taskCompleted(currentRequest, true);
            } else {
                TaskCompleteListener taskCompleteListener = (TaskCompleteListener) mcontext;
                taskCompleteListener.taskCompleted(currentRequest, true);
            }

        }

        ;

    };


    private static class ServerCommunication extends
            AsyncTask<String, String, String> {

        private static ServerCommunication instance;
        private HttpURLConnection urlConnection;
        private String logo = "";
        private String facebook_id = "";
        private String google_id = "";
        private String instagram_access_token = "";

        private String email = "";

        private String password = "";

        private String connected_via = "";
        private String device_type = "";
        private String device_id = "";
        private String device_token = "";

        private String full_name = "";
        Bitmap mIcon11 = null;
        private String postUsrId ="";
        private String pro_pic="";
        private String imgTp="";
        private String postngTitle="";


        public static ServerCommunication getInstance() {
            // if (null == instance) {
            instance = new ServerCommunication();
            // }
            return instance;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (context!=null) {
                    progressDialog = new ProgressDialog(context);
                    //progressDialog = MyCustomProgressDialog.ctor(context);
                   // String msg = context.getString(R.string.prgssWheel);
                    progressDialog.setMessage("Please wait");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                }else {
                    progressDialog = new ProgressDialog(mContext);
                    //progressDialog = MyCustomProgressDialog.ctor(context);
                   // String msg = mContext.getString(R.string.prgssWheel);
                    progressDialog.setMessage("Please wait");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(String responseJson) {
            super.onPostExecute(responseJson);

            long start = System.currentTimeMillis();
            if (networkFlag) {
                if (responseJson != null) {
                    if (responseJson.length() > 0) {
                        if (context!=null){
                        parseStatus = ResponseParserFactory.getInstance()
                                .parseResponse(context, currentRequest,
                                        responseJson);
                        responseJson = null;
                        }else {
                            parseStatus = ResponseParserFactory.getInstance()
                                    .parseResponse(mContext, currentRequest,
                                            responseJson);
                            responseJson = null;
                        }

                    }
                }
            } else if (no_peer_certificate) {

                // Toast.makeText(context, R.string.no_peer, 1).show();
            } else {

                if (context!=null){
                    Toast.makeText(context, "network error", Toast.LENGTH_SHORT)
                            .show();
                }else {
                    Toast.makeText(mContext, "network error", Toast.LENGTH_SHORT)
                            .show();
                }

                sb_error = "network error";
            }

            // Log.v(ConstantLib.LOG, "Server Response : " + responseJson);
            long end = System.currentTimeMillis();
            long diff = end - start;

            Log.e("Post Eecute", "ooooo" + diff);
            handler.sendEmptyMessage(currentRequest);
        }


        @Override
        protected String doInBackground(String... params) {

            URL urll;
            networkFlag = true;
            String responseJson = " ";
            String query = "";
            HttpURLConnection conn = null;
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            int serverResponseCode = 0;

            if ("POST".equals(method)) {
                if (networkFlag) {

                    switch (currentRequest) {

                        /*case ConstantLib.REQ_TYPE_LOGIN_TYPE:
                            postDataParams.put("username",params[0]);
                            postDataParams.put("password",params[1]);

                            break;*/
                        case ConstantLib.REQ_TYPE_SIGN_UP:

                            full_name = params[0];
                            facebook_id = params[1];
                            google_id = params[2];
                            instagram_access_token = params[3];
                            connected_via = params[4];
                            device_type = params[5];
                            device_id = params[6];
                            device_token = params[7];
                            password = params[8];
                            email = params[9];
                            logo = params[10];

                            break;


                    }

                    try {


                        System.setProperty("http.keepAlive", "false");

                        /////////////////////////////////////////////
                        ////UPLOADING PICTURE AND DATA

                       // FileInputStream fileInputStream = new FileInputStream(new File(logo));

                        // open a URL connection to the Servlet
                        FileInputStream fileInputStream = new FileInputStream(logo);

                        //download picture from the source
                       /* InputStream in = new URL(logo).openStream();
                        mIcon11 = BitmapFactory.decodeStream(in);

                        //encoding image into a byte array
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        mIcon11.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] imageBytes = baos.toByteArray();

                        // open a URL connection to the Servlet
                        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(imageBytes);*/
                        urll = new URL(url);

                        // Open a HTTP  connection to  the URL
                        conn = (HttpURLConnection) urll.openConnection();
                        conn.setDoInput(true); // Allow Inputs
                        conn.setDoOutput(true); // Allow Outputs

                        conn.setUseCaches(false); // Don't use a Cached Copy
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Accept-Encoding", "");
                        conn.setRequestProperty("Connection", "Keep-Alive");
                        //conn.setRequestProperty("ENCTYPE", "application/json");
                        conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                        //conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                        conn.setRequestProperty("facebook_id",facebook_id);
                        conn.setRequestProperty("google_id",google_id);
                        conn.setRequestProperty("instagram_access_token", instagram_access_token);
                        conn.setRequestProperty("full_name",full_name);
                        conn.setRequestProperty("email", email);
                        conn.setRequestProperty("password", password);
                        conn.setRequestProperty("profile_pic", logo);
                        conn.setRequestProperty("connected_via", connected_via);
                        conn.setRequestProperty("device_type", device_type);
                        conn.setRequestProperty("device_id", device_id);
                        conn.setRequestProperty("device_token", device_token);




                        dos = new DataOutputStream(conn.getOutputStream());

                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"full_name\"" + lineEnd + lineEnd
                                + full_name + lineEnd);

                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"facebook_id\"" + lineEnd + lineEnd
                                + facebook_id + lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"google_id\"" + lineEnd + lineEnd
                                + google_id + lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"instagram_access_token\"" + lineEnd + lineEnd
                                + instagram_access_token + lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"connected_via\"" + lineEnd + lineEnd
                                + connected_via + lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"device_type\"" + lineEnd + lineEnd
                                + device_type + lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"device_id\"" + lineEnd + lineEnd
                                + device_id + lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"device_token\"" + lineEnd + lineEnd
                                + device_token + lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"password\"" + lineEnd + lineEnd
                                + password + lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"email\"" + lineEnd + lineEnd
                                + email + lineEnd);





                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"profile_pic\";filename=\""
                                + logo + "\"" + lineEnd);
                        dos.writeBytes("Content-Type: image/jpeg" + lineEnd);

                        dos.writeBytes(lineEnd);

                        // create a buffer of  maximum size
                        bytesAvailable = fileInputStream.available();

                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        while (bytesRead > 0) {

                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        }

                        // send multipart form data necesssary after file data...
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                        // Responses from the server (code and message)
                        serverResponseCode = conn.getResponseCode();

                        String responseMsg = conn.getResponseMessage();

                        System.out.println("jsonResponseeeeeeeeeeeeeeeee-------------" + responseJson);

                        Log.i("uploadFile", "HTTP Response is : "
                                + responseMsg + ": " + serverResponseCode);

                        //close the streams //
                        fileInputStream.close();
                        dos.flush();
                        dos.close();

                        int responseCode = conn.getResponseCode();

                        System.out.println("requesttttttttttttttttt codeeeeeeeeeee===========>" + responseCode);

                        if (responseCode == HttpsURLConnection.HTTP_OK) {
                            String line;
                            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            while ((line = br.readLine()) != null) {
                                responseJson += line;

                                System.out.println("Responceeeeeeeeeeee" + line);
                            }
                        } else {
                            responseJson = "";

                            //throw new HttpException(responseCode+"");
                            System.out.println("Errorrrrrrrrrrrrrrrrrrrrrrrrrrrr ");
                        }
                        conn.disconnect();


                       /* urll = new URL(url);

                        //HttpURLConnection conn = (HttpURLConnection) urll.openConnection();
                        conn.setReadTimeout(ConstantLib.READTIMEOUT);
                        conn.setConnectTimeout(ConstantLib.CONNECTION_TIMEOUT);
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Connection", "Keep-Alive");
                        conn.setRequestProperty("Cache-Control", "no-cache");
                        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" +boundary);
                        conn.setDoInput(true);
                        conn.setDoOutput(true);

                        OutputStream os = conn.getOutputStream();
                        BufferedWriter writer = new BufferedWriter(
                                new OutputStreamWriter(os, "UTF-8"));

                        HashMap<String, String> postDataParams = new HashMap<String,String>();

                        switch (currentRequest) {

                            case ConstantLib.REQ_TYPE_LOGIN_TYPE:
                                postDataParams.put("username",params[0]);
                                postDataParams.put("password",params[1]);

                                break;
                            case ConstantLib.REQ_TYPE_SIGN_UP:

                                postDataParams.put("fullname",params[0]);
                                postDataParams.put("nameoftype",params[1]);
                                postDataParams.put("selectcountry",params[2]);
                                postDataParams.put("areauser",params[3]);
                                postDataParams.put("cityuser",params[4]);
                                postDataParams.put("streetuser",params[5]);
                                postDataParams.put("chooseyournamepage",params[6]);
                                postDataParams.put("chooseusername",params[7]);
                                postDataParams.put("passworduser",params[8]);
                                postDataParams.put("repassworduser",params[9]);
                                postDataParams.put("emailuser",params[10]);
                                postDataParams.put("mobile",params[11]);
                                postDataParams.put("selectcategory",params[12]);
                                postDataParams.put("gender",params[13]);
                                postDataParams.put("bday",params[14]);
                                postDataParams.put("logo",params[15]);


                                break;


                        }

                        writer.write(getPostDataString(postDataParams));

                        writer.flush();
                        writer.close();

                        os.close();

                        int responseCode=conn.getResponseCode();

                        System.out.println("requesttttttttttttttttt codeeeeeeeeeee===========>"+responseCode);

                        if (responseCode == HttpsURLConnection.HTTP_OK) {
                            String line;
                            BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            while ((line=br.readLine()) != null) {
                                responseJson+=line;

                                System.out.println("Responceeeeeeeeeeee"+line);
                            }
                        }
                        else {
                            responseJson="";

                            //throw new HttpException(responseCode+"");
                            System.out.println("Errorrrrrrrrrrrrrrrrrrrrrrrrrrrr ");
                        }
*/
                    } catch (ConnectTimeoutException e) {

                        networkFlag = false;
                        e.printStackTrace();
                    } catch (SocketTimeoutException e) {

                        networkFlag = false;
                        e.printStackTrace();
                    } catch (SSLPeerUnverifiedException ex) {
                        // TODO: handle exception
                        networkFlag = false;
                        no_peer_certificate = true;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    responseJson = null;
                }

            } else {

                // for Get method
                if (networkFlag) {
                    switch (currentRequest) {

                        /*case ConstantLib.REQ_TYPE_LOGIN_TYPE:
                            postDataParams.put("username",params[0]);
                            postDataParams.put("password",params[1]);

                            break;*/
                        case ConstantLib.REQ_TYPE_POST_IMAGES:

                            postUsrId = params[0];
                            pro_pic = params[1];
                            imgTp = params[2];
                            postngTitle = params[3];

                            break;


                    }

                    try {


                    System.setProperty("http.keepAlive", "false");

                    /////////////////////////////////////////////
                    ////UPLOADING PICTURE AND DATA

                    // FileInputStream fileInputStream = new FileInputStream(new File(logo));

                    // open a URL connection to the Servlet
                    FileInputStream fileInputStream = new FileInputStream(pro_pic);

                    //download picture from the source
                       /* InputStream in = new URL(logo).openStream();
                        mIcon11 = BitmapFactory.decodeStream(in);

                        //encoding image into a byte array
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        mIcon11.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] imageBytes = baos.toByteArray();

                        // open a URL connection to the Servlet
                        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(imageBytes);*/
                    urll = new URL(url);

                    // Open a HTTP  connection to  the URL
                    conn = (HttpURLConnection) urll.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs

                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Accept-Encoding", "");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    //conn.setRequestProperty("ENCTYPE", "application/json");
                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    //conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty("posting_user_id",postUsrId);
                    conn.setRequestProperty("attachment",pro_pic);
                    conn.setRequestProperty("type", imgTp);
                    conn.setRequestProperty("title",postngTitle);




                    dos = new DataOutputStream(conn.getOutputStream());

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"posting_user_id\"" + lineEnd + lineEnd
                            + postUsrId + lineEnd);

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"type\"" + lineEnd + lineEnd
                            + imgTp + lineEnd);
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"title\"" + lineEnd + lineEnd
                            + postngTitle + lineEnd);

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"attachment\";filename=\""
                            + pro_pic + "\"" + lineEnd);
                    dos.writeBytes("Content-Type: image/jpeg" + lineEnd);

                    dos.writeBytes(lineEnd);

                    // create a buffer of  maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {

                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    }

                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();

                    String responseMsg = conn.getResponseMessage();

                    System.out.println("jsonResponseeeeeeeeeeeeeeeee-------------" + responseJson);

                    Log.i("uploadFile", "HTTP Response is : "
                            + responseMsg + ": " + serverResponseCode);

                    //close the streams //
                    fileInputStream.close();
                    dos.flush();
                    dos.close();

                    int responseCode = conn.getResponseCode();

                    System.out.println("requesttttttttttttttttt codeeeeeeeeeee===========>" + responseCode);

                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        while ((line = br.readLine()) != null) {
                            responseJson += line;

                            System.out.println("Responceeeeeeeeeeee" + line);
                        }
                    } else {
                        responseJson = "";

                        //throw new HttpException(responseCode+"");
                        System.out.println("Errorrrrrrrrrrrrrrrrrrrrrrrrrrrr ");
                    }
                    conn.disconnect();


                } catch (ConnectTimeoutException e) {

                    networkFlag = false;
                    e.printStackTrace();
                } catch (SocketTimeoutException e) {

                    networkFlag = false;
                    e.printStackTrace();
                } catch (SSLPeerUnverifiedException ex) {
                    // TODO: handle exception
                    networkFlag = false;
                    no_peer_certificate = true;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                responseJson = null;
            }


                /*if (networkFlag) {



                    switch (currentRequest) {

                        case ConstantLib.REQ_TYPE_LOGIN_TYPE:
                            //postDataParams.put("login_key",params[0]);
*//*
                            try {
                               *//**//* query = String.format("username=%s&password=%s",
                                        URLEncoder.encode(params[0], charset),
                                        URLEncoder.encode(params[1], charset));*//**//*

                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }*//*

                            break;


                    }
                    try {
                        urll = new URL(url+query);

                        System.out.println("urlllllllllllllllllllllll"+urll.toString());

                        urlConnection = (HttpURLConnection) urll.openConnection();
                        int responseCode = urlConnection.getResponseCode();

                        if (responseCode == HttpsURLConnection.HTTP_OK) {
                            String responseString = readStream(urlConnection.getInputStream());
                            Log.v("CatalogClient", responseString);
                            // response = new JSONArray(responseString);
                            responseJson = responseString;
                        } else {
                            Log.v("CatalogClient", "Response code:" + responseCode);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (urlConnection != null)
                            urlConnection.disconnect();
                    }
                }*/
            }


            return responseJson;
        }

        private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }

            System.out.println("Urllllllllllll" + result.toString());

            return result.toString();
        }

        private String readStream(InputStream in) {
            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }
}
