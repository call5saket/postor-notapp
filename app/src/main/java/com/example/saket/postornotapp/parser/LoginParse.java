package com.example.saket.postornotapp.parser;



import com.example.saket.postornotapp.entity.LoginEntityBean;
import com.example.saket.postornotapp.view.ForgetPasswordActivity;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mobiweb on 29/9/15.
 */
public class LoginParse {

    private static LoginParse instance;
    private boolean parseStatus = true;

    ArrayList<LoginEntityBean> response = new ArrayList<LoginEntityBean>();
    ArrayList<LoginEntityBean>val = new ArrayList<LoginEntityBean>();

    public static LoginParse getInstance(){
        if(null== instance){
            instance = new LoginParse();
        }

        return instance;

    }
    public boolean parseResponse(String jsonResponse){
        parseStatus = true;
        JSONObject jsonObj;
        try{
            jsonObj = new JSONObject(jsonResponse);

            LoginEntityBean login=new LoginEntityBean();

            login.setSuccess(jsonObj.getString("message"));
            //SignInActivity.suc=jsonObj.getString("message");
            ForgetPasswordActivity.suc=jsonObj.getString("message");




            response.add(login);
            LoginEntityBean.setInstance(login);
            LoginEntityBean.setValue(response);
            //  LoginEntityBean.setChildren(response);

		/*
			JSONArray jsoArr = jsonObj.getJSONArray(ConstantLib.result);
			for (int i=0;i<jsoArr.length();i++){
			try{
				LoginEntityBean leb = new LoginEntityBean();

				JSONObject c = jsoArr.getJSONObject(i);

				leb.setId(c.getString(ConstantLib.id));
				val.add(leb);
				LoginEntityBean.setInstance(leb);
				LoginEntityBean.setValue(val);


			}catch (Exception e){
				e.printStackTrace();
				parseStatus = false;
			}
			}*/

        }catch(Exception e){
            e.printStackTrace();
            parseStatus = false;
        }

        return parseStatus;

    }
}
