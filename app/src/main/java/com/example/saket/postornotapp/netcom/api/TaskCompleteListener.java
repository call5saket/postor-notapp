/**
 * 
 */
package com.example.saket.postornotapp.netcom.api;

/**
 * @author Yogesh tatwal
 * 
 */
public interface TaskCompleteListener {

	public void taskCompleted(int taskType, boolean status);

}
