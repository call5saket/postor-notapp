package com.example.saket.postornotapp.entity;

import java.util.ArrayList;

/**
 * Created by Saket on 02/01/2016.
 */
public class UserDetailEntity {

    private static UserDetailEntity instance;

    int code ;
    public String message ="",user_id="",facebook_id="",google_id="",instagram_access_token="",full_name="",email="",password="",password_reset_key="",profile_pic="",
            gender="",description="",phone_number="",connected_via="",user_status="",device_type="",device_id="",device_token="",date_created="";


    public static ArrayList<UserDetailEntity> value;

    public static UserDetailEntity getInstance() {

        if (instance ==null){

            instance = new UserDetailEntity();
        }
        return instance;
    }

    public static void setInstance(UserDetailEntity instance) {
        UserDetailEntity.instance = instance;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getGoogle_id() {
        return google_id;
    }

    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    public String getInstagram_access_token() {
        return instagram_access_token;
    }

    public void setInstagram_access_token(String instagram_access_token) {
        this.instagram_access_token = instagram_access_token;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_reset_key() {
        return password_reset_key;
    }

    public void setPassword_reset_key(String password_reset_key) {
        this.password_reset_key = password_reset_key;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getConnected_via() {
        return connected_via;
    }

    public void setConnected_via(String connected_via) {
        this.connected_via = connected_via;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public static ArrayList<UserDetailEntity> getValue() {
        return value;
    }

    public static void setValue(ArrayList<UserDetailEntity> value) {
        UserDetailEntity.value = value;
    }
}
